//
//  HeartDartViewController.h
//  HeartDart
//
//  Created by tasol on 10/30/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HeartDartViewController : UIViewController

-(void)showAlert:(NSString *)title Content:(NSString *)bodyText;
-(void)showAlert:(NSString *)title Content:(NSString *)bodyText Parent:(id)parent;
-(void)callBack:class;
-(void)callBack:Class withObject:(id)sender;
-(void)callBack:Class image:(id)image originalImage:(id)originalImage;


@end
