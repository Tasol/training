//
//  MasterViewController.m
//  HeartDart
//
//  Created by tasol on 10/29/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "MasterViewController.h"
#import "AppDelegate.h"
#import "MasterViewCell.h"
#import "CustomNavigationBar.h"
#import "iJoomerDataProvider.h"
#import "HeartDartOnBoardingViewController.h"
#import "LoginViewController.h"
@interface MasterViewController ()

@end

@implementation MasterViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self setNavigationBar];
    
    arrStream=[[NSMutableArray alloc]initWithObjects:@"arr1",@"arr2",@"arr3",@"arr4", nil];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self checkLogin];
}

-(void)checkLogin
{
    @try
    {
        if([ApplicationConfiguration sharedInstance].isLogedIn)
        {
            NSLog(@"Master View controller");
            
        }
        else
        {
            iJoomerDataProvider *controller=[[iJoomerDataProvider alloc]init];
            BOOL success=[controller checkLogin];
            if(success)
            {
                BOOL isValidate=[controller validateLogin];
                if(!isValidate)
                {
                    LoginViewController *controller_=[[LoginViewController alloc]init];
                    [self.navigationController pushViewController:controller_ animated:YES];
                    return;
                }
                NSLog(@"Master View controller");
            }
            else
            {
                NSUserDefaults *standardUserDefault=[NSUserDefaults standardUserDefaults];
                NSString *userName=[standardUserDefault valueForKey:@"email"];
                NSString *userPass=[standardUserDefault valueForKey:@"password"];
                
                if(userName && userPass)
                {
                    LoginViewController *controller_=[[LoginViewController alloc]init];
                    [self.navigationController pushViewController:controller_ animated:YES];
                    
                }else{
                    HeartDartOnBoardingViewController *controller_=[[HeartDartOnBoardingViewController alloc]init];
                    [self.navigationController pushViewController:controller_ animated:YES];
                }
                return;
            }
        }
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.btnSideMenu.hidden = NO;
}

-(void)setNavigationBar
{
    @try
    {
        CustomNavigationBar *navBar=[[CustomNavigationBar alloc]init];
        [navBar setTitle:@"Stream"];
        [navBar setRightSideButtonImage:[UIImage imageNamed:@"btn_new.png"]];
        [self.view addSubview:navBar];
        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
        [navBar bringSubviewToFront:appDelegate.btnSideMenu];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrStream count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 200;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MasterViewCell *cell=(MasterViewCell *)[tblStream dequeueReusableCellWithIdentifier:@"MasterViewCell"];
    @try {
        if (cell==nil) {
            cell=[[MasterViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"MasterViewCell" target:self];
            cell.selectionStyle=UITableViewCellSelectionStyleNone;
            [cell setBackgroundColor:[UIColor clearColor]];
        }
        
//        cell.textLabel.text=[arrStream objectAtIndex:indexPath.row];
        [cell reload];
//        [cell.btnReject addTarget:self action:@selector(btnRejectPressed:) forControlEvents:UIControlEventTouchUpInside];
//        rejectId=[[[arrRequests objectAtIndex:indexPath.row] valueForKey:@"offerID"] intValue];
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    return cell;
}

-(IBAction)btnFriendPressed:(id)sender
{
    @try
    {
        btnFriend.selected=YES;
        btnFollowing.selected=NO;
        btnRandom.selected=NO;
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(IBAction)btnFollowingPressed:(id)sender
{
    @try
    {
        btnFollowing.selected=YES;
        btnFriend.selected=NO;
        btnRandom.selected=NO;
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(IBAction)btnRandomPressed:(id)sender
{
    @try
    {
        btnRandom.selected=YES;
        btnFriend.selected=NO;
        btnFollowing.selected=NO;
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

@end
