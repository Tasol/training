//
//  LoginViewController.m
//  HeartDart
//
//  Created by tasol on 10/29/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "LoginViewController.h"
#import "HeartDartDataProvider.h"
#import "MasterViewController.h"
#import "iJoomerDataProvider.h"
#import "CustomNavigationBar.h"
#import "HeartDartOnBoardingViewController.h"
#import "AppDelegate.h"
@interface LoginViewController ()

@end

@implementation LoginViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self=[super initWithNibName:[NSString stringWithFormat:@"LoginViewController%@",[ApplicationConfiguration sharedInstance].POSTFIX_XIB] bundle:nil];
    if (self) {
        // Custom initialization
    }
    return self;

}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTheme];
    [self setNavigationBar];

}

-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication]delegate];
    appDelegate.btnSideMenu.hidden=YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)setNavigationBar
{
    @try
    {
        CustomNavigationBar *navBar=[[CustomNavigationBar alloc]init];
        [navBar setTitle:@"Login"];
        [navBar setLeftSideButtonText:@"Back" withTargate:self action:@selector(btnBackPressed) forEvent:UIControlEventTouchUpInside];
        [self.view addSubview:navBar];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
}

-(void)btnBackPressed
{
    @try
    {
        [self.navigationController pushViewController:[[HeartDartOnBoardingViewController alloc]init] animated:YES];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}
#pragma Textview/TextField delegate methods ---------------------------------------------

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    [UIView animateWithDuration:1.0f animations:^{
        viewContainer.contentOffset = CGPointMake(0, 100);
    }];
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:txtEmail])
    {
        [txtPassword becomeFirstResponder];
    }
    else
    {
        [textField resignFirstResponder];
        [UIView animateWithDuration:1.0f animations:^{
            viewContainer.contentOffset = CGPointMake(0, 0);
        }];
    }
    return YES;
}


-(void)setTheme
{
    @try
    {
        UIView *paddingView=[[UIView alloc]initWithFrame:CGRectMake(20, 10, 10, 35)];
        txtEmail.leftView=paddingView;
        txtEmail.leftViewMode=UITextFieldViewModeAlways;
        
        paddingView=[[UIView alloc]initWithFrame:CGRectMake(20, 10, 10, 35)];
        txtPassword.leftView=paddingView;
        txtPassword.leftViewMode=UITextFieldViewModeAlways;
        
        

        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

#pragma Validate Method-------------------------------------------

-(BOOL)validate
{
    @try
    {
        if ([txtEmail.text isEqualToString:@""])
        {
            [super showAlert:@"Email" Content:@"Email should not be blank"];
            [txtEmail becomeFirstResponder];
            return FALSE;
        }
        
        else if([txtPassword.text isEqualToString:@""])
        {
            [super showAlert:@"PassWord" Content:@"Please Enter Your PassWord"];
            [txtPassword becomeFirstResponder];
            return FALSE;
        }
        
        else if (![txtEmail.text isEqualToString:@""]){
            if ([self validateEmailWithString:txtEmail.text]) {
                
            }
            else{
                [super showAlert:@"InValid Email" Content:@"Email Address is not valid"];
                [txtEmail becomeFirstResponder];
                return FALSE;
            }
            
            
        }
        
    
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    return TRUE;
    
}

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}




-(IBAction)btnLoginPressed:(id)sender
{
    @try
    {
//        MasterViewController *registerView=[[MasterViewController alloc]init];
//        [self.navigationController pushViewController:registerView animated:YES];
        if ([self validate])
        {
            [spinner startAnimating];
            [self performSelector:@selector(loginProcess) withObject:self afterDelay:0.1];
            
            
        }
        
        
    }
    @catch (NSException *exception) {
        
    }
    @finally
    {
        
    }
    
    
}
    
-(IBAction)btnforgotPasswordPressed:(id)sender
{
    @try
    {
        UIAlertView *alertview=[[UIAlertView alloc]initWithTitle:@"Forgot your Password" message:@"Enter Email address" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"OK", nil];
        alertview.tag=1;
        alertview.alertViewStyle=UIAlertViewStylePlainTextInput;
        [alertview show];
        
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
   
}


-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    @try
    {
        if(alertView.tag==1 && buttonIndex==1)
        {
            UITextField *txtEmailField=[alertView textFieldAtIndex:0];
            strForgotLoginEmail=txtEmailField.text;
            [spinner startAnimating];
            [self performSelector:@selector(forgotLoginProcess) withObject:nil afterDelay:0.1];
        }
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}


-(void)loginProcess
{
    @try
    {
        NSMutableDictionary *postVariable=[[NSMutableDictionary alloc]init];
        [postVariable setValue:txtEmail.text forKey:@"email"];
        [postVariable setValue:txtPassword.text forKey:@"password"];
        
        BOOL success=[[HeartDartDataProvider sharedInstance]loginUser:postVariable];
        
        if (success)
        {
//            MasterViewController *masterView=[[MasterViewController alloc]init];
            [self.navigationController popToRootViewControllerAnimated:YES];
            
        }
        
//        else
//        {
//            [super showAlert:@"Alert" Content:@"Email Address and Password not match"];
//        }
        
        
        
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        [spinner stopAnimating];
        
    }
}


-(void)forgotLoginProcess
{
    @try
    {
        BOOL success=NO;
        
        NSDictionary *taskData=[[NSDictionary alloc]initWithObjectsAndKeys:strForgotLoginEmail,@"email", nil];
        success=[iJoomerDataProvider forgotLogin:taskData];
        if(success)
        {
            UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"New password sent" message:@"Please check your email for new password" delegate:nil cancelButtonTitle:nil otherButtonTitles:@"ok", nil];
            [alert show];
        }
        [spinner stopAnimating];
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}



@end
