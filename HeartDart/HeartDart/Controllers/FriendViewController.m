//
//  FriendViewController.m
//  HeartDart
//
//  Created by tasol on 11/5/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "FriendViewController.h"
#import "FriendListCell.h"
#import "UserListViewController.h"
#import "AppDelegate.h"
#import "HeartDartDataProvider.h"
#import "CustomNavigationBar.h"
#import "FriendDetailViewController.h"
#import "ApplicationConfiguration.h"
@interface FriendViewController ()

@end

@implementation FriendViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [collectionViewFriend registerClass:[FriendListCell class] forCellWithReuseIdentifier:@"FriendListCell"];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate *appDelegate=(AppDelegate*)[[UIApplication sharedApplication]delegate];
    appDelegate.btnSideMenu.hidden=NO;
    [self setNavigationBar];
  

}

-(void)viewWillAppear:(BOOL)animated
{
    [spinner startAnimating];
    
    [self getFriend];

//    [self performSelector:@selector(getFriend) withObject:self afterDelay:1.0];

}

-(void)setNavigationBar
{
    @try
    {
        CustomNavigationBar *navBar=[[CustomNavigationBar alloc]init];
        [navBar setTitle:@"Friends"];
        [navBar setRightSideButtonImage:[UIImage imageNamed:@"btn_new.png"]];
        [navBar.btnRightSideButton addTarget:self action:@selector(btnAddPressed) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:navBar];
        
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}


-(void)btnAddPressed
{
    @try
    {
        UserListViewController *userList=[[UserListViewController alloc]init];
        [self.navigationController pushViewController:userList animated:YES];
        
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}




-(void)getFriend
{
    @try
    {
        isFriend=1;
        
        NSMutableDictionary *postVariable=[[NSMutableDictionary alloc]init];
        [postVariable setValue:[NSString stringWithFormat:@"%d",isFriend] forKey:@"friendsOnly"];
        arrFriend=[[HeartDartDataProvider sharedInstance]getUserList:postVariable ];

//        arrFriendRequest=[[NSMutableArray alloc]initWithObjects:@"arr",@"arr1", nil];
        arrFriendRequest=[HeartDartDataProvider sharedInstance].arrFriendRequest;
        [collectionViewFriend reloadData];
        [ self setView ];
        
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        [spinner stopAnimating];
        
    }
}

-(void)setView
{
    
    if (arrFriendRequest.count==0)
    {
        scrollView.hidden=YES;
        collectionViewFriend.frame=CGRectMake(0, 64, 320, 504);
        
        
        
    }
    else
    {
        scrollView.hidden=NO;
        collectionViewFriend.frame=CGRectMake(0, 161, 320, 404);
        [self setFriendRequestView];
    }

}
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *reuseidentifier = @"FriendListCell";
    FriendListCell *cell;
    @try
    {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:reuseidentifier forIndexPath:indexPath];
        cell.layer.shouldRasterize = YES;
        cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
        
        cell.postDetails = [arrFriend objectAtIndex:indexPath.row];
        [cell reloadCell];
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    return cell;
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [arrFriend count];
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(92, 92);
}

- (UIEdgeInsets)collectionView:
(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(5, 10, 10, 10);
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
        FriendDetailViewController *friendDetail=[[FriendDetailViewController alloc]init];
        [self.navigationController pushViewController:friendDetail animated:YES];
        friendDetail.FriendDetail=[arrFriend objectAtIndex:indexPath.row];
    }
    @catch (NSException *exception)
    {
    }
    @finally
    {
        
    }
    
    
}

-(void)setFriendRequestView
{
    @try
    {
        int arrayLength=arrFriendRequest.count;
        NSMutableDictionary *mutableDict;
        for (int i=0;i<arrayLength;i++) {
            mutableDict=[[NSMutableDictionary alloc]initWithDictionary:[arrFriendRequest objectAtIndex:i]];
            [mutableDict valueForKey:@"email"];
                   }
        
        for (int i=arrFriendRequest.count; i>0; i--)
    
        {
           

            UIView *view=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 320, 98)];
            view.backgroundColor=[NSUtil colorWithHexString:[mutableDict valueForKey:@"moodColor"]];
            
            [scrollView addSubview:view];
            
            UIImageView *imgProfile=[[UIImageView alloc]initWithFrame:CGRectMake(15,15, 60, 60)];
            imgProfile.clipsToBounds=YES;
            imgProfile.layer.cornerRadius=imgProfile.frame.size.width/2;
            imgProfile.backgroundColor=[UIColor whiteColor];
            imgProfile.layer.borderWidth=2.0f;
            imgProfile.layer.borderColor=[UIColor whiteColor].CGColor;
            imgProfile.image=[NSUtil cropImageFromMiddle:[NSUtil getImageFromURL:[mutableDict valueForKey:@"avatar"]]];
            [view addSubview:imgProfile];
            
            
            UILabel *lblMessage=[[UILabel alloc]initWithFrame:CGRectMake(100, 15, 200, 50)];
            lblMessage.numberOfLines=2;
            lblMessage.textColor=[UIColor blackColor];
            lblMessage.text=[HeartDartDataProvider sharedInstance].stringMessage;
            [view addSubview:lblMessage];
            
            
            UIButton *btnAccept=[[UIButton alloc]initWithFrame:CGRectMake(80, 60, 91, 31)];
            [btnAccept setImage:[UIImage imageNamed:@"btn_accept_off.png"] forState:UIControlStateNormal];
            [btnAccept addTarget:self action:@selector(btnAcceptPressed) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:btnAccept];
            
            UIButton *btnDecline=[[UIButton alloc]initWithFrame:CGRectMake(200, 60, 91, 31)];
            [btnDecline setImage:[UIImage imageNamed:@"btn_decline_off.png"] forState:UIControlStateNormal];
            [btnDecline addTarget:self action:@selector(btnDeclinePressed) forControlEvents:UIControlEventTouchUpInside];
            [view addSubview:btnDecline];

            
        }

    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)btnAcceptPressed
{
    @try
    {
        [self showAlert:@"Accept Friend Request?" Content:@"Becoming Friend with this user will able you to send send message directly to each other"];
        alert.tag=1;
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)btnDeclinePressed
{
    @try
    {
        [self showAlert:@"Decline Your Friend Request" Content:@"If you decline this friend request you will not able to send the message directly to each other"];
        
        alert.tag=2;
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)showAlert:(NSString *)titleValue Content:(NSString *)content
{
    @try
    {
        alert=[[UIAlertView alloc]initWithTitle:titleValue message:content delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Confirm", nil];
        [alert show];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
//    if (buttonIndex==1 && alert.tag==1)
//    {
//        [self friendAccept];
//        
//    }
//    
//    else if (buttonIndex==2 && alert.tag==2)
//    {
//        [self friendDecline];
//    }
}


-(void)friendAccept
{
    @try
    {
        NSMutableDictionary *postVariable=[[NSMutableDictionary alloc]init];
        [postVariable setValue:[ApplicationConfiguration sharedInstance].restID     forKeyPath:@"userID"];
        [postVariable setValue:@"accept" forKeyPath:@"response"];
        [[HeartDartDataProvider sharedInstance] responseToFriendRequest:postVariable];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
}

-(void)friendDecline
{
    @try
    {
        NSMutableDictionary *postVariable=[[NSMutableDictionary alloc]init];
        [postVariable setValue:[ApplicationConfiguration sharedInstance].restID     forKeyPath:@"userID"];
        [postVariable setValue:@"decline" forKeyPath:@"response"];
        [[HeartDartDataProvider sharedInstance] responseToFriendRequest:postVariable];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }

    
}



@end
