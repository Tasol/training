//
//  MyProfileViewController.m
//  HeartDart
//
//  Created by tasol on 11/3/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "MyProfileViewController.h"
#import "CustomNavigationBar.h"
#import "AppDelegate.h"
#import "ijoomerDataProvider.h"
#import "RegisterViewController.h"
#import "HeartDartDataProvider.h"
@interface MyProfileViewController ()

@end

@implementation MyProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self=[super initWithNibName:[NSString stringWithFormat:@"MyProfileViewController%@",[ApplicationConfiguration sharedInstance].POSTFIX_XIB] bundle:nil];
    if (self)
    {
        
        userDetails =[[NSDictionary alloc]init];
//        userId=[[NSString alloc]init];
        
    }
    return self;
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self setNavigationBar];
//    userId=[ApplicationConfiguration sharedInstance].restID;
  
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.btnSideMenu.hidden=NO;
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
     [self getProfile];
}
-(void)setNavigationBar
{
    @try
    {
        CustomNavigationBar *navBar=[[CustomNavigationBar alloc]init];
        [navBar setTitle:@"My Profile"];
        [navBar setRightSideButtonText:@"Save" withTargate:self action:nil forEvent:UIControlEventTouchUpInside];
        [self.view addSubview:navBar];
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)getProfile
{
    @try
    {
        NSMutableDictionary *postVariable=[[NSMutableDictionary alloc]init ];
        [postVariable setValue:[ApplicationConfiguration sharedInstance].restID forKey:@"userID"];
       userDetails= [[HeartDartDataProvider sharedInstance]getProfile:postVariable];
        if (!(userDetails==nil))
        {
            [self setValue];
        }
        

    }
    @catch (NSException *exception) {
        
    }
    @finally
    {
        
    }
}

-(void)setValue
{
    
//    NSString *image=[userDetails valueForKey:@"avatar"];
    
    imgProfile.image=[NSUtil cropImageFromMiddle:[NSUtil getImageFromURL:[userDetails valueForKey:@"avatar"]]];
    lblMessageSent.text=[userDetails valueForKey:@"msgSent"];
    lblMessageRecived.text=[NSString stringWithFormat:@"%ld",(long)[[userDetails valueForKey:@"msgReceived"] integerValue]];
    lblFriends.text=[userDetails valueForKey:@"friendsCount"];
    
}

-(IBAction)btnEditImagePressed:(id)sender

{
    @try
    {
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

-(IBAction)btnEditProfilePressed:(id)sender
{
    @try
    {
        AppDelegate *appDelegate=(AppDelegate *)[[UIApplication sharedApplication] delegate ];
        appDelegate.btnSideMenu.hidden=YES;
        RegisterViewController *controller=[[RegisterViewController alloc]init];
        [self.navigationController pushViewController:controller animated:YES];
       
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

//-(void)updateProfile
//{
//    @try
//    {
//        NSMutableDictionary *dict=[[NSMutableDictionary alloc]init];
//        [dict setValue:txtStatus.text forKey:@"status"];
//                
//        BOOL success=[[HeartDartDataProvider sharedInstance] updatedProfile:dict];
//        if (success)
//        {
//            
//        }
//        
//        
//    }
//    @catch (NSException *exception)
//    {
//        
//    }
//    @finally
//    {
//        
//    }
//}


@end
