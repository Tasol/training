//
//  FriendDetailViewController.h
//  HeartDart
//
//  Created by tasol on 11/5/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendDetailViewController : HeartDartViewController<UIAlertViewDelegate>
{
    IBOutlet UIImageView *imgBackground;
    IBOutlet UIImageView *imgProfile;
    IBOutlet UILabel *lblName;
    IBOutlet UITextView *txtViewStatus;
    IBOutlet UIButton *btnAddFriend;
    IBOutlet UIButton *btnMessage;
    IBOutlet UIButton *btnFollow;
    IBOutlet UIButton *btnStatus;
    IBOutlet UIButton *btnMessage1;

}
-(IBAction)btnStatusPressed:(id)sender;
-(IBAction)btnMessage1Pressed:(id)sender;
-(IBAction)btnAddFriendPressed:(id)sender;
-(IBAction)btnMessagePressed:(id)sender;
-(IBAction)btnFollowPressed:(id)sender;
@property (nonatomic,retain)NSDictionary *FriendDetail;

@end
