//
//  RegisterViewController.h
//  HeartDart
//
//  Created by tasol on 10/29/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RegisterViewController : HeartDartViewController<UITextFieldDelegate,UIScrollViewDelegate,UIActionSheetDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

{
    IBOutlet UITextField *txtUserName;
    IBOutlet UITextField *txtEmail ;
    IBOutlet UITextField *txtPassword ;
    IBOutlet UITextField *txtconfirmPassword;
    IBOutlet UIScrollView *viewContainer;
    IBOutlet UIButton *btnRegister;
    IBOutlet UIImageView *imgProfile ;
    IBOutlet UIButton *btnAddPhoto;
    IBOutlet UIActivityIndicatorView *spinner;
    IBOutlet UILabel *lblPassWord;
    IBOutlet UILabel *lblConfirmPassWord;
    
    NSDictionary *userDetails;
    NSData *imgData;
}

-(IBAction)btnRegisterPressed:(id)sender;
-(IBAction)btnAddPhotoPressed:(id)sender;
@end
