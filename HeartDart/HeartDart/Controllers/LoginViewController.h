//
//  LoginViewController.h
//  HeartDart
//
//  Created by tasol on 10/29/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : HeartDartViewController<UITextFieldDelegate,UIScrollViewDelegate>

{
    IBOutlet UITextField *txtEmail ;
    IBOutlet UITextField *txtPassword ;
    IBOutlet UIButton *btnLogin ;
    IBOutlet UIButton *btnforgotPassword ;
    IBOutlet UIScrollView *viewContainer;
    IBOutlet UIActivityIndicatorView *spinner;
    
    NSString *strForgotLoginEmail;

}

- (IBAction)btnLoginPressed:(id)sender;
- (IBAction)btnforgotPasswordPressed:(id)sender ;
@end
