//
//  MyProfileViewController.h
//  HeartDart
//
//  Created by tasol on 11/3/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfileViewController : HeartDartViewController
{
    IBOutlet UIButton *btnEditImage;
    IBOutlet UIButton *btnEditProfile;
    IBOutlet UITextView *txtStatus;
    IBOutlet UILabel *lblMessageSent;
    IBOutlet UILabel *lblMessageRecived;
    IBOutlet UILabel *lblFriends;
    IBOutlet UIImageView *imgProfile;
    IBOutlet UIImageView *imgCircleMarker;
    
    NSString *userId;
    NSDictionary *userDetails;

}

-(IBAction)btnEditImagePressed:(id)sender;
-(IBAction)btnEditProfilePressed:(id)sender;
@end
