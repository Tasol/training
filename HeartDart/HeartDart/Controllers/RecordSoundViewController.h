//
//  RecordSoundViewController.h
//  HeartDart
//
//  Created by tasol on 10/31/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordSoundViewController : UIViewController
{
    IBOutlet UIButton *btnListen;
    IBOutlet UIButton *btnReRecord;
    IBOutlet UIButton *btnAddRecording;
    IBOutlet UIButton *btnRecord;
    
    IBOutlet UILabel *lblPressToRecord;
    IBOutlet UIImageView *imgBackground;
}

-(IBAction)btnListenPressed:(id)sender;
-(IBAction)btnReRecordPressed:(id)sender;
-(IBAction)btnAddRecordingPressed:(id)sender;
-(IBAction)btnRecordPressed:(id)sender;
@end
