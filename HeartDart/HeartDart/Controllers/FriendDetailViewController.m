//
//  FriendDetailViewController.m
//  HeartDart
//
//  Created by tasol on 11/5/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//
#define kBorderWidth 3.0
#define kCornerRadius 10.0
#import "FriendDetailViewController.h"
#import "CustomNavigationBar.h"
#import "ApplicationConfiguration.h"
#import "HeartDartDataProvider.h"
@interface FriendDetailViewController ()

@end

@implementation FriendDetailViewController
@synthesize FriendDetail;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated
{
    [self setNavigationBar];
    [self setValue];
}

-(void)setNavigationBar
{
    @try
    {
        CustomNavigationBar *navBar=[[CustomNavigationBar alloc]init];
        [navBar setTitle:[FriendDetail valueForKey:@"username"]];
        [navBar setRightSideButtonText:@"Back" withTargate:self action:@selector(btnBackPressed) forEvent:UIControlEventTouchUpInside];
        [self.view addSubview:navBar];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)btnBackPressed
{
    @try
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)setValue
{
    @try
    {
        
        lblName.text=[FriendDetail valueForKey:@"username"];
        [lblName setTextColor:[NSUtil colorWithHexString:[FriendDetail valueForKey:@"moodColor"]]];
        
        txtViewStatus.text=[FriendDetail valueForKey:@"status"];
         [txtViewStatus setBackgroundColor:[NSUtil colorWithHexString:[FriendDetail valueForKey:@"moodColor"]]];
        txtViewStatus.textColor=[UIColor whiteColor];
        
        
        imgProfile.image=[NSUtil cropImageFromMiddle:[NSUtil getImageFromURL:[FriendDetail valueForKey:@"avatar"]]];
        imgProfile.layer.cornerRadius=(imgProfile.frame.size.width)/2;
        imgProfile.clipsToBounds=YES;
        imgProfile.layer.borderWidth=2.0f;
        imgProfile.layer.borderColor=[NSUtil colorWithHexString:[FriendDetail valueForKey:@"moodColor"]].CGColor;
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(IBAction)btnStatusPressed:(id)sender
{
    @try
    {
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}
-(IBAction)btnMessage1Pressed:(id)sender
{
    @try
    {
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}
-(IBAction)btnAddFriendPressed:(id)sender
{
    @try
    {
        
        UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"Send Friend Request?" message:@"Becoming Friend with this user will able to send messages directly to each other" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Send", nil];
        [alert show];
       
        
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}
-(IBAction)btnMessagePressed:(id)sender
{
    @try
    {
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}
-(IBAction)btnFollowPressed:(id)sender
{
    @try
    {
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex==1)
    {
        [self sendFriendRequest];
    }
}

-(void)sendFriendRequest
{
    @try
    {
        NSMutableDictionary *postVariable=[[NSMutableDictionary alloc]init];
        [postVariable setValue:[FriendDetail valueForKey:@"userID"] forKey:@"userID"];
        
        BOOL success=[[HeartDartDataProvider sharedInstance] sendFriendRequest:postVariable];
        if (success)
        {
            
        }
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
}


@end
