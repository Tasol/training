//
//  NewMessageViewController.h
//  HeartDart
//
//  Created by tasol on 10/31/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewMessageViewController : UIViewController

{
    IBOutlet UIButton *btnNext;
    IBOutlet UIButton *btnAddMedia;
    IBOutlet UITextView *txtEnterMessage;
    IBOutlet UITextView *txtAuthor;
    IBOutlet UITextView *txtURL;
    IBOutlet UIScrollView *scrollView;
    
    IBOutlet UIImageView *imgMessage;
}
-(IBAction)btnAddMediaPressed:(id)sender;
-(IBAction)btnNextPressed:(id)sender;

@end
