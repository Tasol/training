//
//  MessageMediaViewController.h
//  HeartDart
//
//  Created by tasol on 10/31/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MessageMediaViewController : UIViewController
{
    IBOutlet UIButton *btnUploadPhotoOrVideo;
    IBOutlet UIButton *btnTakePhoto;
    IBOutlet UIButton *btnRecordVideo;
    IBOutlet UIButton *btnAddYoutubeVideo;
    IBOutlet UIButton *btnUploadSoundFile;
    IBOutlet UIButton *btnRecordSound;
    
    IBOutlet UIButton *btnBrowseLibrary;
    
    IBOutlet UIImageView *imgBackground;
    IBOutlet UIScrollView *scrollView;
    
    
    
    
}

-(IBAction)btnUploadPhotoOrVideoPressed:(id)sender;
-(IBAction)btnTakePhotoPressed:(id)sender;
-(IBAction)btnRecordVideoPressed:(id)sender;
-(IBAction)btnAddYoutubeVideoPressed:(id)sender;
-(IBAction)btnUploadSoundFilePressed:(id)sender;
-(IBAction)btnRecordSoundPressed:(id)sender;
-(IBAction)btnBrowseLibraryPressed:(id)sender;


@end