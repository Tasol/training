//
//  SelectUserViewController.h
//  HeartDart
//
//  Created by tasol on 11/3/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectUserCell.h"

@interface SelectUserViewController : UIViewController<UITableViewDelegate , UITableViewDataSource>

{
    IBOutlet UITableView *tblSelectedUser;
    IBOutlet UIButton *btnSend;
    IBOutlet UIButton *btnAddToLibrary;
}

-(IBAction)btnSendPressed:(id)sender;
-(IBAction)btnAddToLibraryPressed:(id)sender;

@end
