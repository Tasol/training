//
//  SelectUserViewController.m
//  HeartDart
//
//  Created by tasol on 11/3/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "SelectUserViewController.h"

@interface SelectUserViewController ()

@end

@implementation SelectUserViewController



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self=[super initWithNibName:@"SelectUserViewControllerLower" bundle:nil];
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)btnAddToLibraryPressed:(id)sender
{
    @try {
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
    
    }
}

-(IBAction)btnSendPressed:(id)sender
{
    @try {
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

#pragma mark 
#pragma mark Tableview delegate methods


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 105;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
 
    static NSString *reuseIdentifier = @"SelectUserCell";
    SelectUserCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    @try
    {
        if (cell==nil)
        {
            cell = [[SelectUserCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseIdentifier];
        }
        [cell reloadCell];
        
    }
    @catch (NSException *exception) {
        
    }
    @finally
    {
        
    }
    return cell;
}

@end
