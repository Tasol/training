//
//  RecordVideoViewController.h
//  HeartDart
//
//  Created by tasol on 10/31/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RecordVideoViewController : UIViewController
{
    
        IBOutlet UIButton *btnAddVideo;
        IBOutlet UIButton *btnRecordVideo;
        IBOutlet UIImageView *imgRecordVideo;
}
-(IBAction)btnAddVideoPressed:(id)sender;
-(IBAction)btnRecordVideoPressed:(id)sender;


@end
