//
//  CustomNavigationBar.m
//  Intafy
//
//  Created by tasol on 2/13/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "NSUtil.h"
#import <QuartzCore/QuartzCore.h>
#import "CustomNavigationBar.h"

@implementation CustomNavigationBar
@synthesize lblTitle,btnRightSideButton,btnLeftSideButton;
- (id)init
{
    
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        self =[super initWithFrame:CGRectMake(0, 0, 768, 60)];
    }else if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        if([[UIDevice currentDevice].systemVersion floatValue]>=7.0)
        {
            self =[super initWithFrame:CGRectMake(0, 0, 320, 64)];
            imgNavigationBG = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 64)];
        }
        else
        {
            self =[super initWithFrame:CGRectMake(0, 0, 320, 44)];
            imgNavigationBG = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 320, 44)];
        }
    
    }
    
    if (self)
    {
        
//        imgNavigationBG.image = [UIImage imageNamed:@"topbar.png"];
        imgNavigationBG.backgroundColor=[UIColor whiteColor];
        [self addSubview:imgNavigationBG];
      
        lblTitle = [[UILabel alloc] initWithFrame:CGRectMake(110, self.frame.size.height -30, 100, 30)];
        
        lblTitle.textAlignment=NSTextAlignmentCenter;
        [lblTitle setFont:[UIFont fontWithName:nil size:25.0]];
        lblTitle.textColor=[UIColor colorWithRed:0.0/255.0 green:204.0/255.0 blue:190.0/255.0 alpha:1.0];
        [lblTitle setBackgroundColor:[UIColor clearColor]];
        [self addSubview:lblTitle];
//        if (UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPad) {
//            imgBottomLine=[[UIImageView alloc]initWithFrame:CGRectMake(0.0, self.frame.size.height-1, 768, 1.0)];
////
////        }
//        else{
        imgBottomLine = [[UIImageView alloc] initWithFrame:CGRectMake(0, 63,320, 1)];
//        }
        setFrame(imgBottomLine, NSViewHorizontalAlignmentNatural, NSViewHorizontalAlignmentNatural, FALSE, FALSE);
       
        [imgBottomLine setBackgroundColor:[UIColor colorWithRed:0.0/255.0 green:204.0/255.0 blue:190.0/255.0 alpha:1.0]];
        [self addSubview:imgBottomLine];
        
//        [self setBackgroundColor:[LGOMGlobalTheme sharedInstance].getFontColorRed];
    }
    return self;
}


-(void)setTitle:(NSString*)title{
    if(lblTitle){
        CGSize size = [title sizeWithFont:[lblTitle font]];
        lblTitle.frame=CGRectMake(([[UIScreen mainScreen] bounds].size.width-size.width)/2, self.frame.size.height -7-34, size.width, lblTitle.frame.size.height);
        setFrame(lblTitle, NSViewHorizontalAlignmentNatural, NSViewHorizontalAlignmentNatural, FALSE, FALSE);
        lblTitle.text=title;
    }
}

-(void)setRightSideButtonText:(NSString*)text withTargate:(id)target action:(SEL)selector forEvent:(UIControlEvents)event
{
    CGSize size;
    @try
    {
//        [text sizeWithFont:[btnRightSideButton.titleLabel font]];
        if (!btnRightSideButton)
        {
            btnRightSideButton = [UIButton buttonWithType:UIButtonTypeCustom];
//            [self setFrameOfComponents];
        }
        else
        {
//        [btnRightSideButton removeta]
        }
        btnRightSideButton.titleLabel.font = [UIFont fontWithName:nil size:18];
        size = [self widthOfString:text withFont:btnRightSideButton.titleLabel.font];
        btnRightSideButton.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width-10-size.width,22, size.width+5,30);
        
        btnRightSideButton.hidden=NO;
        [btnRightSideButton setTitle:text forState:UIControlStateNormal];
        [btnRightSideButton setTitleColor:[UIColor colorWithRed:0.0/255.0 green:204.0/255.0 blue:190.0/255.0 alpha:1.0] forState:UIControlStateHighlighted];
        [btnRightSideButton setTitleColor:[UIColor colorWithRed:0.0/255.0 green:204.0/255.0 blue:190.0/255.0 alpha:1.0] forState:UIControlStateNormal];
        
        btnRightSideButton.titleLabel.textAlignment=NSTextAlignmentRight;
        [btnRightSideButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentRight];
        [btnRightSideButton addTarget:target action:selector forControlEvents:event];
        [self addSubview:btnRightSideButton];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
- (CGSize)widthOfString:(NSString *)string withFont:(UIFont *)font {
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName, nil];
    return [[[NSAttributedString alloc] initWithString:string attributes:attributes] size];
}
-(void)setRightSideButtonImageFromURL:(NSString*)imageURL
{
    @try {
        if (!btnRightSideButton) {
            btnRightSideButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self setFrameOfComponents];
        }else{
            //        [btnRightSideButton removeta]
        }
        UIImage *image =[NSUtil getImageFromURL:imageURL];
        btnRightSideButton.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width-10-34, 25, 34, 34);
        btnRightSideButton.layer.borderWidth=1;
        btnRightSideButton.layer.borderColor = [ApplicationConfiguration sharedInstance].buttonBorderColor.CGColor;
        btnRightSideButton.layer.cornerRadius = 17;
        btnRightSideButton.clipsToBounds=YES;
        [btnRightSideButton setBackgroundImage:image forState:UIControlStateNormal];
        [self addSubview:btnRightSideButton];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

-(void)setRightSideButtonImage:(UIImage*)image
{
    @try {
        if (!btnRightSideButton) {
            btnRightSideButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self setFrameOfComponents];
        }else
        {
            //        [btnRightSideButton removeta]
        }

        btnRightSideButton.frame = CGRectMake([[UIScreen mainScreen] bounds].size.width-10-30, 20, 30, 30);

        [btnRightSideButton setBackgroundImage:image forState:UIControlStateNormal];
        [self addSubview:btnRightSideButton];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

-(void)setLeftSideButtonImage:(UIImage *)image
{
    @try
    {
        if (!btnLeftSideButton)
        {
            btnLeftSideButton = [UIButton buttonWithType:UIButtonTypeCustom];
            [self setFrameOfComponents];
        }
        else
        {
            //        [btnRightSideButton removeta]
        }
        [btnLeftSideButton setFrame:CGRectMake(10, 23, image.size.width+5, image.size.height+5)];
        [btnLeftSideButton setTitle:@"" forState:UIControlStateNormal];
        [btnLeftSideButton setBackgroundImage:image forState:UIControlStateNormal];
        [self addSubview:btnLeftSideButton];
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

-(void)removeTargetFromRightSideButton:(id)target action:(SEL)selector forEvent:(UIControlEvents)event
{
    @try
    {
        if (btnRightSideButton)
        {
            [btnRightSideButton removeTarget:target action:selector forControlEvents:event];
        }
    }
    @catch (NSException *exception)
    {
        
    }
    @finally {
        
    }
}

-(void)removeTargetFromLeftSideButton:(id)target action:(SEL)selector forEvent:(UIControlEvents)event
{
    @try {
        if (btnLeftSideButton) {
            [btnLeftSideButton removeTarget:target action:selector forControlEvents:event];
            btnLeftSideButton.hidden=YES;
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(void)setLeftSideButtonText:(NSString*)text withTargate:(id)target action:(SEL)selector forEvent:(UIControlEvents)event
{
    CGSize size;
    @try {
        if (!btnLeftSideButton) {
            btnLeftSideButton = [UIButton buttonWithType:UIButtonTypeCustom];
//            [self setFrameOfComponents];
         }
         size = [self widthOfString:text withFont:btnLeftSideButton.titleLabel.font];
        btnLeftSideButton.frame=CGRectMake(10, 22,size.width+5,30);
        btnLeftSideButton.hidden=NO;
        [btnLeftSideButton setTitle:text forState:UIControlStateNormal];
//        btnLeftSideButton.titleLabel.textColor = ;
        [btnLeftSideButton setTitleColor:[UIColor colorWithRed:0.0/255.0 green:204.0/255.0 blue:190.0/255.0 alpha:1.0] forState:UIControlStateHighlighted];
        [btnLeftSideButton setTitleColor:[UIColor colorWithRed:0.0/255.0 green:204.0/255.0 blue:190.0/255.0 alpha:1.0] forState:UIControlStateNormal];

        [btnLeftSideButton setContentHorizontalAlignment:UIControlContentHorizontalAlignmentLeft];
        btnLeftSideButton.titleLabel.textAlignment=NSTextAlignmentLeft;
        [btnLeftSideButton addTarget:target action:selector forControlEvents:event];
        [self addSubview:btnLeftSideButton];
        
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}
-(void)setFrameOfComponents{
    @try {
        
        if(lblTitle)
        {
            NSLog(@"LABEL TEXT :-%@",btnLeftSideButton.titleLabel.text);
            if(btnLeftSideButton&& btnLeftSideButton.titleLabel.text!=NULL)
            {
                btnLeftSideButton.frame=CGRectMake(10, self.frame.size.height -7-25, (([[UIScreen mainScreen] bounds].size.width-lblTitle.frame.size.width)/2)-25,25);
            }
            if(btnRightSideButton)
            {
                btnRightSideButton.frame=CGRectMake(lblTitle.frame.size.width+lblTitle.frame.origin.x, self.frame.size.height -7-25, (([[UIScreen mainScreen] bounds].size.width-lblTitle.frame.size.width)/2)-25,25);
            }
        }
        else
        {
            if(btnLeftSideButton && ![btnLeftSideButton.titleLabel.text isEqualToString:@""])
                btnLeftSideButton.frame=CGRectMake(10, self.frame.size.height -7-25,( ([[UIScreen mainScreen] bounds].size.width)/2)-10,25);
            if(btnRightSideButton)
                btnRightSideButton.frame=CGRectMake(0, self.frame.size.height -7-25, (([[UIScreen mainScreen] bounds].size.width)/2)-10,25);
            
        }
//        setFrame(btnLeftSideButton, NSViewHorizontalAlignmentNatural, NSViewHorizontalAlignmentNatural, FALSE, FALSE);
//        setFrame(btnRightSideButton, NSViewHorizontalAlignmentNatural, NSViewHorizontalAlignmentNatural, FALSE, FALSE);
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}
-(void)deleteLeftSideButton{
    @try {
        [btnLeftSideButton removeFromSuperview];
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}
-(void)deleteRightSideButton{
    @try {
        [btnRightSideButton removeFromSuperview];
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}
@end
