//
//  Core_joomer.m
//  Core_joomer
//
//  Created by Tailored Solutions on 11/6/12.
//  Copyright (c) 2013 tailored. All rights reserved.
//

#import "Core_joomer.h"
//#import "Encrypt_Decrypt.h"




@implementation Core_joomer


//Get Dictionary from URL.
+ (NSDictionary *) sendRequest:(NSString *)jsonstring 
{
     NSDictionary *responseData = [NSDictionary dictionaryWithObjectsAndKeys:@"Connection Error.\n Please check internet..",@"message",@"25",@"code",nil];
    @try {
        //nsmutable data.
        NSMutableData *postBody = [NSMutableData data];
        NSString *stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
        NSLog(@"jsonstring:-%@",jsonstring);
        //string to data.
        const char *datatoUpload = [jsonstring UTF8String];
        NSData *requestData = [NSData dataWithBytes: datatoUpload length: strlen(datatoUpload)];
        NSLog(@"jsonstring:-%@",[[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
        
        //encrypt data.
        //requestData = [Encrypt_Decrypt AES128EncryptWithKey:requestData];
        
        //request send code.
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: [ApplicationConfiguration sharedInstance].SERVER_URL]];
        
        NSLog(@"URL : %@",[ApplicationConfiguration sharedInstance].SERVER_URL);
        
        
        // add body to post
        
        [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[@"Content-Disposition: form-data; name=\"reqObject\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding ]];
        
        [postBody appendData:requestData];
        //[postBody appendData:[comment dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        // set Content-Type in HTTP header
        stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", stringBoundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        //post request and data.
        //    NSString *postLength = [NSString stringWithFormat:@"%d", [requestData length]];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postBody length]];
        [request setHTTPMethod: @"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        //[request setHTTPBody: requestData];
        [request setHTTPBody: postBody];
        [request setTimeoutInterval:600];
        
        NSError *e = nil;

        NSHTTPURLResponse *urlResponse = nil;
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:&urlResponse error:&e];
        NSLog(@"Response Code :-%ld",(long)[urlResponse statusCode]);
        if ([urlResponse statusCode]!=200 && ([urlResponse statusCode]==0||[urlResponse statusCode]==404||[urlResponse statusCode]==500)){
            NSMutableDictionary *returnData = [[NSMutableDictionary alloc] init];
            [returnData setObject:[NSString stringWithFormat:@"%ld",(long)[urlResponse statusCode]] forKey:@"code"];
            return returnData;
        }
     
        if (returnData != nil) {
            
            //connection done and get data.
            
            DLog(@"Http Response:\n%@",[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]);
            NSError *localError;
            
            if(localError==nil){
                responseData = [NSJSONSerialization JSONObjectWithData:returnData options:0 error:&localError];
                return responseData;
            }
            
        }

    }
    @catch (NSException *exception) {
        DLog(@"Exception:%@",exception);
    }
    @finally {
        
    }
    
    return responseData;
}

//for voice and image

+ (NSDictionary *) dict_withVoice:(NSString *) jsonstring Imaagedata :(NSData *)imagedata Voicedata :(NSData *)voicedata static_URL:(NSString *)URL Voicename:(NSString *) voicename;
{
        
    NSMutableData *postBody = [NSMutableData data];
    NSString *stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    
    //string to data.
    NSLog(@"jsonstring:-%@",jsonstring);
    //string to data.
    const char *datatoUpload = [jsonstring UTF8String];
    NSData *requestData = [NSData dataWithBytes: datatoUpload length: strlen(datatoUpload)];
    NSLog(@"jsonstring:-%@",[[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
    // add body to post
    
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Disposition: form-data; name=\"reqObject\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding ]];
    
    [postBody appendData:requestData];
    //[postBody appendData:[comment dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    /////////////****************************voice.
    
    //Image
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Disposition: form-data; name=\"image\"; filename=\"item.png\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Type: image/png\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postBody appendData:imagedata];
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // final boundary
    [postBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    //end image
    
    
    
    //Voice
    
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"voice\"; filename=\"voice.%@\"\r\n",@"aac"] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Type: voice/aac\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postBody appendData:voicedata];
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // final boundary
    [postBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    ////////////*******************************end voice.
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: URL]];
    
    
    // set Content-Type in HTTP header
    stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", stringBoundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    //post request and data.
    //NSString *postLength = [NSString stringWithFormat:@"%d", [requestData length]];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postBody length]];
    [request setHTTPMethod: @"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    //[request setHTTPBody: requestData];
    [request setHTTPBody: postBody];
    [request setTimeoutInterval:600];
    
    
    NSError *e = nil;
    
    double starttime = [[NSDate date] timeIntervalSince1970];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&e];
    double endtime = [[NSDate date] timeIntervalSince1970];
    
    double timeDiffInSecs = endtime - starttime;
    NSDictionary *resultsDictionary1;
    if (returnData != nil) {
        
        //connection done and get data.
        
        //decrypt data.
        //returnData = [Encrypt_Decrypt AES128EncryptWithKey:requestData];
        
        NSString *returnPage = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        DLog(@"Http Response:\n%@",returnPage);
        
        NSError *localError;
        resultsDictionary1 = [NSJSONSerialization JSONObjectWithData:returnData options:0 error:&localError];
        //log dictionary set.
        
        NSMutableDictionary *dict_log = [[NSMutableDictionary alloc] init];
        
        [dict_log setObject:@"test strig" forKey:@"req_url_json_string"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",starttime] forKey:@"req_start_timestamp"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",endtime] forKey:@"req_end_timestamp"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",timeDiffInSecs] forKey:@"req_diff_timestamp"];
        
        NSString *strtmptest = [NSString stringWithFormat:@"%@",[resultsDictionary1 objectForKey:@"code"]];
        strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (strtmptest == (id)[NSNull null] || strtmptest.length < 1 || [strtmptest isEqualToString:@"(null)"])
        {
            
            [dict_log setObject:@"500" forKey:@"req_code"];
        }
        else{
            
            strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if (strtmptest.length > 0) {
                
                [dict_log setObject:[resultsDictionary1 objectForKey:@"code"] forKey:@"req_code"];
            }
            else {
                
                [dict_log setObject:@"500" forKey:@"req_code"];
            }
        }
        
        if (static_Logcashing_flag == YES) {
            
            NSArray *arrayObj = [[NSUserDefaults standardUserDefaults] objectForKey:@"log_array"];
            NSMutableArray *arrtemplog;
            if ([arrayObj count] > 0) {
                
                arrtemplog = [[NSMutableArray alloc] initWithArray:arrayObj];
                [arrtemplog addObject:dict_log];
                
            }
            else {
                
                arrtemplog = [[NSMutableArray alloc] init];
                [arrtemplog addObject:dict_log];
            }
            [[NSUserDefaults standardUserDefaults] setObject:arrtemplog forKey:@"log_array"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"log_array"];
        }
        
        
        //log dictionary set end.
        
        
    }
    return resultsDictionary1;
    
}

//Get dectionary from URL with image.
+ (NSDictionary *) sendRequest:(NSString *)jsonstring Imaagedata :(NSData *)imagedata
{
    
    NSMutableData *postBody = [NSMutableData data];
    NSString *stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    
    //string to data.
    NSLog(@"jsonstring:-%@",jsonstring);
    //string to data.
    const char *datatoUpload = [jsonstring UTF8String];
    NSData *requestData = [NSData dataWithBytes: datatoUpload length: strlen(datatoUpload)];
    NSLog(@"jsonstring:-%@",[[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
    
    // add body to post
    
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Disposition: form-data; name=\"reqObject\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding ]];
    
    [postBody appendData:requestData];
    //[postBody appendData:[comment dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    /////////////****************************image.
    
    //image
    
    stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Disposition: form-data; name=\"image\"; filename=\"item.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Type: image/jpeg\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // get the image data from main bundle directly into NSData object
    //    NSData *imgData = UIImageJPEGRepresentation(item, 1.0);
    // add it to body
    [postBody appendData:imagedata];
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // final boundary
    [postBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    ////////////*******************************end image.
    
    
    //encrypt data.
    //requestData = [Encrypt_Decrypt AES128EncryptWithKey:requestData];
    
    //request send code.
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: [ApplicationConfiguration sharedInstance].SERVER_URL]];
    
     NSLog(@"URL : %@",[ApplicationConfiguration sharedInstance].SERVER_URL);
    
    // set Content-Type in HTTP header
    stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", stringBoundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    //post request and data.
    //NSString *postLength = [NSString stringWithFormat:@"%d", [requestData length]];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postBody length]];
    [request setHTTPMethod: @"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    //[request setHTTPBody: requestData];
    [request setHTTPBody: postBody];
    [request setTimeoutInterval:600];
    
    
    NSError *e = nil;
    
    double starttime = [[NSDate date] timeIntervalSince1970];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&e];
    double endtime = [[NSDate date] timeIntervalSince1970];
    
    double timeDiffInSecs =  endtime - starttime;
    
    if (returnData != nil) {
        
        //connection done and get data.
        //decrypt data.
        //returnData = [Encrypt_Decrypt AES128EncryptWithKey:requestData];
        
        NSString *returnPage = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        DLog(@"Http Response:\n%@",returnPage);
        
        NSDictionary *resultsDictionary1;
        
        NSError *localError;
        resultsDictionary1 = [NSJSONSerialization JSONObjectWithData:returnData options:0 error:&localError];
        //log dictionary set.
        
        NSMutableDictionary *dict_log = [[NSMutableDictionary alloc] init];
        
        [dict_log setObject:@"test strig" forKey:@"req_url_json_string"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",starttime] forKey:@"req_start_timestamp"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",endtime] forKey:@"req_end_timestamp"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",timeDiffInSecs] forKey:@"req_diff_timestamp"];
        
        NSString *strtmptest = [NSString stringWithFormat:@"%@",[resultsDictionary1 objectForKey:@"code"]];
        strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (strtmptest == (id)[NSNull null] || strtmptest.length < 1 || [strtmptest isEqualToString:@"(null)"])
        {
            
            [dict_log setObject:@"500" forKey:@"req_code"];
        }
        else{
            
            strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if (strtmptest.length > 0) {
                
                [dict_log setObject:[resultsDictionary1 objectForKey:@"code"] forKey:@"req_code"];
            }
            else {
                
                [dict_log setObject:@"500" forKey:@"req_code"];
            }
        }
        
        if (static_Logcashing_flag == YES) {
            
            NSArray *arrayObj = [[NSUserDefaults standardUserDefaults] objectForKey:@"log_array"];
            NSMutableArray *arrtemplog;
            if ([arrayObj count] > 0) {
                
                arrtemplog = [[NSMutableArray alloc] initWithArray:arrayObj];
                [arrtemplog addObject:dict_log];
                
            }
            else {
                
                arrtemplog = [[NSMutableArray alloc] init];
                [arrtemplog addObject:dict_log];
            }
            [[NSUserDefaults standardUserDefaults] setObject:arrtemplog forKey:@"log_array"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"log_array"];
        }
        
        
        //log dictionary set end.
        
        return resultsDictionary1;
    }
    else {
        
        NSDictionary *resultsDictionary1 = [NSDictionary dictionaryWithObjectsAndKeys:@"Connection Error.\n Please check internet..",@"message",@"25",@"code",nil];
        return resultsDictionary1;
    }
    
    return nil;
}
+ (NSDictionary *) sendRequestForSingleImagePost:(NSString *)jsonstring Imagedata :(NSData *)imagedata
{
    
    NSMutableData *postBody = [NSMutableData data];
    NSString *stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    
    //string to data.
    NSLog(@"jsonstring:-%@",jsonstring);
    //string to data.
    const char *datatoUpload = [jsonstring UTF8String];
    NSData *requestData = [NSData dataWithBytes: datatoUpload length: strlen(datatoUpload)];
    NSLog(@"jsonstring:-%@",[[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
    
    // add body to post
    
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Disposition: form-data; name=\"reqObject\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding ]];
    
    [postBody appendData:requestData];
    //[postBody appendData:[comment dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    /////////////****************************image.
    
    //image
    
    stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Disposition: form-data; name=\"image_main\"; filename=\"item.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Type: image/jpeg\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // get the image data from main bundle directly into NSData object
    //    NSData *imgData = UIImageJPEGRepresentation(item, 1.0);
    // add it to body
    [postBody appendData:imagedata];
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // final boundary
    [postBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    ////////////*******************************end image.
    
    
    //encrypt data.
    //requestData = [Encrypt_Decrypt AES128EncryptWithKey:requestData];
    
    //request send code.
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: [ApplicationConfiguration sharedInstance].SERVER_URL]];
    
    NSLog(@"URL : %@",[ApplicationConfiguration sharedInstance].SERVER_URL);
    
    // set Content-Type in HTTP header
    stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", stringBoundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    //post request and data.
    //NSString *postLength = [NSString stringWithFormat:@"%d", [requestData length]];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postBody length]];
    [request setHTTPMethod: @"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    //[request setHTTPBody: requestData];
    [request setHTTPBody: postBody];
    [request setTimeoutInterval:600];
    
    
    NSError *e = nil;
    
    double starttime = [[NSDate date] timeIntervalSince1970];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&e];
    double endtime = [[NSDate date] timeIntervalSince1970];
    
    double timeDiffInSecs =  endtime - starttime;
    
    if (returnData != nil) {
        
        //connection done and get data.
        //decrypt data.
        //returnData = [Encrypt_Decrypt AES128EncryptWithKey:requestData];
        
        NSString *returnPage = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        DLog(@"Http Response:\n%@",returnPage);
        
        NSDictionary *resultsDictionary1;
        
        NSError *localError;
        resultsDictionary1 = [NSJSONSerialization JSONObjectWithData:returnData options:0 error:&localError];
        //log dictionary set.
        
        NSMutableDictionary *dict_log = [[NSMutableDictionary alloc] init];
        
        [dict_log setObject:@"test strig" forKey:@"req_url_json_string"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",starttime] forKey:@"req_start_timestamp"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",endtime] forKey:@"req_end_timestamp"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",timeDiffInSecs] forKey:@"req_diff_timestamp"];
        
        NSString *strtmptest = [NSString stringWithFormat:@"%@",[resultsDictionary1 objectForKey:@"code"]];
        strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (strtmptest == (id)[NSNull null] || strtmptest.length < 1 || [strtmptest isEqualToString:@"(null)"])
        {
            
            [dict_log setObject:@"500" forKey:@"req_code"];
        }
        else{
            
            strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if (strtmptest.length > 0) {
                
                [dict_log setObject:[resultsDictionary1 objectForKey:@"code"] forKey:@"req_code"];
            }
            else {
                
                [dict_log setObject:@"500" forKey:@"req_code"];
            }
        }
        
        if (static_Logcashing_flag == YES) {
            
            NSArray *arrayObj = [[NSUserDefaults standardUserDefaults] objectForKey:@"log_array"];
            NSMutableArray *arrtemplog;
            if ([arrayObj count] > 0) {
                
                arrtemplog = [[NSMutableArray alloc] initWithArray:arrayObj];
                [arrtemplog addObject:dict_log];
                
            }
            else {
                
                arrtemplog = [[NSMutableArray alloc] init];
                [arrtemplog addObject:dict_log];
            }
            [[NSUserDefaults standardUserDefaults] setObject:arrtemplog forKey:@"log_array"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"log_array"];
        }
        
        
        //log dictionary set end.
        
        return resultsDictionary1;
    }
    else {
        
        NSDictionary *resultsDictionary1 = [NSDictionary dictionaryWithObjectsAndKeys:@"Connection Error.\n Please check internet..",@"message",@"25",@"code",nil];
        return resultsDictionary1;
    }
    
    return nil;
}
+(NSDictionary * )sendRequest:(NSString *)jsonstring ImageArray:(NSArray *)imageArray
{
    @try
    {
        NSMutableData *postBody = [NSMutableData data];
        NSString *stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
        
        //string to data.
        NSLog(@"jsonstring:-%@",jsonstring);
        //string to data.
        const char *datatoUpload = [jsonstring UTF8String];
        NSData *requestData = [NSData dataWithBytes: datatoUpload length: strlen(datatoUpload)];
        NSLog(@"jsonstring:-%@",[[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
        
        // add body to post
        
        [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[@"Content-Disposition: form-data; name=\"reqObject\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding ]];
        
        [postBody appendData:requestData];
        //[postBody appendData:[comment dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
//        NSString *postDataString = [NSString stringWithFormat:@"reqObject=%@",jsonstring];
//        [postBody appendData:[postDataString dataUsingEncoding:NSUTF8StringEncoding]];
        
        /////////////****************************image.
        
        //image
        BOOL flag=0;
        for (NSData *imagedata in imageArray)
        {
            stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
            [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            if(flag==0)
            {
               [postBody appendData:[@"Content-Disposition: form-data; name=\"image_main\"; filename=\"item.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                flag=1;
            }
            else
            {
                [postBody appendData:[@"Content-Disposition: form-data; name=\"image_compare\"; filename=\"item1.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
                flag=0;
            }
             [postBody appendData:[@"Content-Type: image/jpeg\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            // get the image data from main bundle directly into NSData object
            //    NSData *imgData = UIImageJPEGRepresentation(item, 1.0);
            // add it to body
            [postBody appendData:imagedata];
            [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            
            // final boundary
            [postBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        }
        
        ////////////*******************************end image.
        
        
        //encrypt data.
        //requestData = [Encrypt_Decrypt AES128EncryptWithKey:requestData];
        
        //request send code.
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: [ApplicationConfiguration sharedInstance].SERVER_URL]];
        
         NSLog(@"URL : %@",[ApplicationConfiguration sharedInstance].SERVER_URL);
        
        // set Content-Type in HTTP header
        stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", stringBoundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        //post request and data.
        //NSString *postLength = [NSString stringWithFormat:@"%d", [requestData length]];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postBody length]];
        [request setHTTPMethod: @"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        //[request setHTTPBody: requestData];
        [request setHTTPBody: postBody];
        [request setTimeoutInterval:600];
        
        
        NSError *e = nil;
        
        double starttime = [[NSDate date] timeIntervalSince1970];
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&e];
        double endtime = [[NSDate date] timeIntervalSince1970];
        
        double timeDiffInSecs =  endtime - starttime;
        
        if (returnData != nil) {
            
            //connection done and get data.
            //decrypt data.
            //returnData = [Encrypt_Decrypt AES128EncryptWithKey:requestData];
            
            NSString *returnPage = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
            DLog(@"Http Response:\n%@",returnPage);
            
            NSDictionary *resultsDictionary1;
            
            NSError *localError;
            resultsDictionary1 = [NSJSONSerialization JSONObjectWithData:returnData options:0 error:&localError];
            //log dictionary set.
            
            NSMutableDictionary *dict_log = [[NSMutableDictionary alloc] init];
            
            [dict_log setObject:@"test strig" forKey:@"req_url_json_string"];
            [dict_log setObject:[NSString stringWithFormat:@"%f",starttime] forKey:@"req_start_timestamp"];
            [dict_log setObject:[NSString stringWithFormat:@"%f",endtime] forKey:@"req_end_timestamp"];
            [dict_log setObject:[NSString stringWithFormat:@"%f",timeDiffInSecs] forKey:@"req_diff_timestamp"];
            
            NSString *strtmptest = [NSString stringWithFormat:@"%@",[resultsDictionary1 objectForKey:@"code"]];
            strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            
            if (strtmptest == (id)[NSNull null] || strtmptest.length < 1 || [strtmptest isEqualToString:@"(null)"])
            {
                
                [dict_log setObject:@"500" forKey:@"req_code"];
            }
            else{
                
                strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
                if (strtmptest.length > 0) {
                    
                    [dict_log setObject:[resultsDictionary1 objectForKey:@"code"] forKey:@"req_code"];
                }
                else {
                    
                    [dict_log setObject:@"500" forKey:@"req_code"];
                }
            }
            
            if (static_Logcashing_flag == YES) {
                
                NSArray *arrayObj = [[NSUserDefaults standardUserDefaults] objectForKey:@"log_array"];
                NSMutableArray *arrtemplog;
                if ([arrayObj count] > 0) {
                    
                    arrtemplog = [[NSMutableArray alloc] initWithArray:arrayObj];
                    [arrtemplog addObject:dict_log];
                    
                }
                else {
                    
                    arrtemplog = [[NSMutableArray alloc] init];
                    [arrtemplog addObject:dict_log];
                }
                [[NSUserDefaults standardUserDefaults] setObject:arrtemplog forKey:@"log_array"];
                [[NSUserDefaults standardUserDefaults] synchronize];
            } else {
                
                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"log_array"];
            }
            
            
            //log dictionary set end.
            
            return resultsDictionary1;
        }
        else {
            
            NSDictionary *resultsDictionary1 = [NSDictionary dictionaryWithObjectsAndKeys:@"Connection Error.\n Please check internet..",@"message",@"25",@"code",nil];
            return resultsDictionary1;
        }
        
        return nil;
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

+ (NSDictionary *) sendRequest:(NSString *)jsonstring Imaagedata :(NSData *)imagedata originalImageData:(NSData *)originalImageData
{
    
    NSMutableData *postBody = [NSMutableData data];
    NSString *stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    
    //string to data.
    NSLog(@"jsonstring:-%@",jsonstring);
    //string to data.
    const char *datatoUpload = [jsonstring UTF8String];
    NSData *requestData = [NSData dataWithBytes: datatoUpload length: strlen(datatoUpload)];
    NSLog(@"jsonstring:-%@",[[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
    
    // add body to post
    
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Disposition: form-data; name=\"reqObject\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding ]];
    
    [postBody appendData:requestData];
    //[postBody appendData:[comment dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    /////////////****************************image.
    
    //image
    
    stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Disposition: form-data; name=\"image\"; filename=\"item.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Type: image/jpeg\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // get the image data from main bundle directly into NSData object
    //    NSData *imgData = UIImageJPEGRepresentation(item, 1.0);
    // add it to body
    [postBody appendData:imagedata];
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // final boundary
    [postBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    ////////////*******************************end image.
    
    /////////////**************************** Originalimage.
    
    //image
    
    stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Disposition: form-data; name=\"orignleImage\"; filename=\"orignleImage.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Type: image/jpeg\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // get the image data from main bundle directly into NSData object
    //    NSData *imgData = UIImageJPEGRepresentation(item, 1.0);
    // add it to body
    [postBody appendData:originalImageData];
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // final boundary
    [postBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    ////////////*******************************end Originalimage.
    
    
    //encrypt data.
    //requestData = [Encrypt_Decrypt AES128EncryptWithKey:requestData];
    
    //request send code.
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: [ApplicationConfiguration sharedInstance].SERVER_URL]];
     NSLog(@"URL : %@",[ApplicationConfiguration sharedInstance].SERVER_URL);
    
    // set Content-Type in HTTP header
    stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", stringBoundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    //post request and data.
    //NSString *postLength = [NSString stringWithFormat:@"%d", [requestData length]];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postBody length]];
    [request setHTTPMethod: @"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    //[request setHTTPBody: requestData];
    [request setHTTPBody: postBody];
    [request setTimeoutInterval:600];
    
    
    NSError *e = nil;
    
    double starttime = [[NSDate date] timeIntervalSince1970];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&e];
    double endtime = [[NSDate date] timeIntervalSince1970];
    
    double timeDiffInSecs =  endtime - starttime;
    
    if (returnData != nil) {
        
        //connection done and get data.
        //decrypt data.
        //returnData = [Encrypt_Decrypt AES128EncryptWithKey:requestData];
        
        NSString *returnPage = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        DLog(@"Http Response:\n%@",returnPage);
        
        NSDictionary *resultsDictionary1;
        
        NSError *localError;
        resultsDictionary1 = [NSJSONSerialization JSONObjectWithData:returnData options:0 error:&localError];
        //log dictionary set.
        
        NSMutableDictionary *dict_log = [[NSMutableDictionary alloc] init];
        
        [dict_log setObject:@"test strig" forKey:@"req_url_json_string"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",starttime] forKey:@"req_start_timestamp"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",endtime] forKey:@"req_end_timestamp"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",timeDiffInSecs] forKey:@"req_diff_timestamp"];
        
        NSString *strtmptest = [NSString stringWithFormat:@"%@",[resultsDictionary1 objectForKey:@"code"]];
        strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (strtmptest == (id)[NSNull null] || strtmptest.length < 1 || [strtmptest isEqualToString:@"(null)"])
        {
            
            [dict_log setObject:@"500" forKey:@"req_code"];
        }
        else{
            
            strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if (strtmptest.length > 0) {
                
                [dict_log setObject:[resultsDictionary1 objectForKey:@"code"] forKey:@"req_code"];
            }
            else {
                
                [dict_log setObject:@"500" forKey:@"req_code"];
            }
        }
        
        if (static_Logcashing_flag == YES) {
            
            NSArray *arrayObj = [[NSUserDefaults standardUserDefaults] objectForKey:@"log_array"];
            NSMutableArray *arrtemplog;
            if ([arrayObj count] > 0) {
                
                arrtemplog = [[NSMutableArray alloc] initWithArray:arrayObj];
                [arrtemplog addObject:dict_log];
                
            }
            else {
                
                arrtemplog = [[NSMutableArray alloc] init];
                [arrtemplog addObject:dict_log];
            }
            [[NSUserDefaults standardUserDefaults] setObject:arrtemplog forKey:@"log_array"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"log_array"];
        }
        
        
        //log dictionary set end.
        
        return resultsDictionary1;
    }
    else {
        
        NSDictionary *resultsDictionary1 = [NSDictionary dictionaryWithObjectsAndKeys:@"Connection Error.\n Please check internet..",@"message",@"25",@"code",nil];
        return resultsDictionary1;
    }
    
    return nil;
}

//VideoUploading...
+ (NSDictionary *) dict_withVideo:(NSString *) jsonstring Videodata :(NSData *)Videodata static_URL :(NSString *)URL videoname:(NSString *) videoname
{

    NSMutableData *postBody = [NSMutableData data];
    NSString *stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    
    NSLog(@"jsonstring:-%@",jsonstring);
    //string to data.
    const char *datatoUpload = [jsonstring UTF8String];
    NSData *requestData = [NSData dataWithBytes: datatoUpload length: strlen(datatoUpload)];
    NSLog(@"jsonstring:-%@",[[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postBody appendData:[@"Content-Disposition: form-data; name=\"reqObject\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding ]];

    [postBody appendData:requestData];
    
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"video\"; filename=\"%@.%@\"\r\n",videoname,@"MOV"] dataUsingEncoding:NSUTF8StringEncoding]];
    
	[postBody appendData:[@"Content-Type: video/quicktime\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
//    [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postBody appendData:Videodata];
    
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: URL]];
    stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", stringBoundary];
    
    //[request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postBody length]];
    [request setHTTPMethod: @"POST"];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    [request setHTTPBody: postBody];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    [request setTimeoutInterval:600];
    
    
    NSError *e = nil;
    
    double starttime = [[NSDate date] timeIntervalSince1970];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&e];
    double endtime = [[NSDate date] timeIntervalSince1970];
    
    double timeDiffInSecs =  endtime - starttime;
    
    if (returnData != nil) {
        
        //connection done and get data.
        
        //decrypt data.
        //returnData = [Encrypt_Decrypt AES128EncryptWithKey:requestData];

        DLog(@"Http Response:\n%@",[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]);
        
        NSDictionary *resultsDictionary1;
        NSError *localError;
        resultsDictionary1 = [NSJSONSerialization JSONObjectWithData:returnData options:0 error:&localError];
        
        NSMutableDictionary *dict_log = [[NSMutableDictionary alloc] init];
        
        [dict_log setObject:@"test strig" forKey:@"req_url_json_string"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",starttime] forKey:@"req_start_timestamp"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",endtime] forKey:@"req_end_timestamp"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",timeDiffInSecs] forKey:@"req_diff_timestamp"];
        
        NSString *strtmptest = [NSString stringWithFormat:@"%@",[resultsDictionary1 objectForKey:@"code"]];
        strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (strtmptest == (id)[NSNull null] || strtmptest.length < 1 || [strtmptest isEqualToString:@"(null)"])
        {
            
            [dict_log setObject:@"500" forKey:@"req_code"];
        }
        else{
            
            strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if (strtmptest.length > 0) {
                
                [dict_log setObject:[resultsDictionary1 objectForKey:@"code"] forKey:@"req_code"];
            }
            else {
                
                [dict_log setObject:@"500" forKey:@"req_code"];
            }
        }
        
        if (static_Logcashing_flag == YES) {
            
            NSArray *arrayObj = [[NSUserDefaults standardUserDefaults] objectForKey:@"log_array"];
            NSMutableArray *arrtemplog;
            if ([arrayObj count] > 0) {
                
                arrtemplog = [[NSMutableArray alloc] initWithArray:arrayObj];
                [arrtemplog addObject:dict_log];
                
            }
            else {
                
                arrtemplog = [[NSMutableArray alloc] init];
                [arrtemplog addObject:dict_log];
            }
            [[NSUserDefaults standardUserDefaults] setObject:arrtemplog forKey:@"log_array"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"log_array"];
        }
        
        
        //log dictionary set end.
        
        return resultsDictionary1;
    }
    else {
        
        NSDictionary *resultsDictionary1 = [NSDictionary dictionaryWithObjectsAndKeys:@"Connection Error.\n Please check internet..",@"message",@"25",@"code",nil];
        return resultsDictionary1;
    }
    
    return nil;
}

//Voice Uploading....
+ (NSDictionary *) sendRequest:(NSString *) jsonstring Voicedata :(NSData *)voicedata
{
    NSMutableData *postBody = [NSMutableData data];
    NSString *stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    
    //string to data.
    NSLog(@"jsonstring:-%@",jsonstring);
    //string to data.
    const char *datatoUpload = [jsonstring UTF8String];
    NSData *requestData = [NSData dataWithBytes: datatoUpload length: strlen(datatoUpload)];
    NSLog(@"jsonstring:-%@",[[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
    
    // add body to post
    
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Disposition: form-data; name=\"reqObject\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding ]];
    
    [postBody appendData:requestData];
    //[postBody appendData:[comment dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    /////////////****************************voice.
    
    //Voice
    
    stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"voice\"; filename=\"%@.%@\"\r\n",@"voice",@"aac"] dataUsingEncoding:NSUTF8StringEncoding]];
    //DLog(@"%@",[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"voice\"; filename=\"%@.%@\"\r\n",voicedata,@"aac"]);
	//[postBody appendData:[@"Content-Type: voice/quicktime\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [postBody appendData:[@"Content-Type: voice/aac\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // get the image data from main bundle directly into NSData object
    //    NSData *imgData = UIImageJPEGRepresentation(item, 1.0);
    // add it to body
    [postBody appendData:voicedata];
    [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    
    // final boundary
    [postBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    ////////////*******************************end voice.
    
    
    //encrypt data.
    //requestData = [Encrypt_Decrypt AES128EncryptWithKey:requestData];
    
    //request send code.
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString:[ApplicationConfiguration sharedInstance].SERVER_URL]];
     NSLog(@"URL : %@",[ApplicationConfiguration sharedInstance].SERVER_URL);
    
//    /////start
//    NSLog(@"SendingRecordedData");
//    NSString *urlString = [NSString stringWithFormat:@"http://your_server/fileupload.php"];
//    NSLog(@"Url:%@",urlString);
//    
//    [request setURL:[NSURL URLWithString:urlString]];
//    
//    //end
    
    // set Content-Type in HTTP header
    stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", stringBoundary];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    //post request and data.
    //NSString *postLength = [NSString stringWithFormat:@"%d", [requestData length]];
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postBody length]];
    [request setHTTPMethod: @"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    //[request setHTTPBody: requestData];
    [request setHTTPBody: postBody];
    [request setTimeoutInterval:600];
    
    
    NSError *e = nil;
    
    double starttime = [[NSDate date] timeIntervalSince1970];
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&e];
    double endtime = [[NSDate date] timeIntervalSince1970];
    
    double timeDiffInSecs =  endtime - starttime;
    
    if (returnData != nil) {
        
        //connection done and get data.
        
        //decrypt data.
        //returnData = [Encrypt_Decrypt AES128EncryptWithKey:requestData];
        
        NSString *returnPage = [[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding];
        DLog(@"Http Response:\n%@",returnPage);
        
        NSDictionary *resultsDictionary1;
        NSError *localError;
        resultsDictionary1 = [NSJSONSerialization JSONObjectWithData:returnData options:0 error:&localError];
        //log dictionary set.
        
        NSMutableDictionary *dict_log = [[NSMutableDictionary alloc] init];
        
        [dict_log setObject:@"test strig" forKey:@"req_url_json_string"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",starttime] forKey:@"req_start_timestamp"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",endtime] forKey:@"req_end_timestamp"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",timeDiffInSecs] forKey:@"req_diff_timestamp"];
        
        NSString *strtmptest = [NSString stringWithFormat:@"%@",[resultsDictionary1 objectForKey:@"code"]];
        strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (strtmptest == (id)[NSNull null] || strtmptest.length < 1 || [strtmptest isEqualToString:@"(null)"])
        {
            
            [dict_log setObject:@"500" forKey:@"req_code"];
        }
        else{
            
            strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if (strtmptest.length > 0) {
                
                [dict_log setObject:[resultsDictionary1 objectForKey:@"code"] forKey:@"req_code"];
            }
            else {
                
                [dict_log setObject:@"500" forKey:@"req_code"];
            }
        }
        
        if (static_Logcashing_flag == YES) {
            
            NSArray *arrayObj = [[NSUserDefaults standardUserDefaults] objectForKey:@"log_array"];
            NSMutableArray *arrtemplog;
            if ([arrayObj count] > 0) {
                
                arrtemplog = [[NSMutableArray alloc] initWithArray:arrayObj];
                [arrtemplog addObject:dict_log];
                
            }
            else {
                
                arrtemplog = [[NSMutableArray alloc] init];
                [arrtemplog addObject:dict_log];
            }
            [[NSUserDefaults standardUserDefaults] setObject:arrtemplog forKey:@"log_array"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"log_array"];
        }
        
        
        //log dictionary set end.
        
        return resultsDictionary1;
    }
    else {
        
        NSDictionary *resultsDictionary1 = [NSDictionary dictionaryWithObjectsAndKeys:@"Connection Error.\n Please check internet..",@"message",@"25",@"code",nil];
        return resultsDictionary1;
    }
    
    return nil;

}

//// return Request time log values.
//+(NSArray *) RequestTimeLog
//{
//    [self CheckDatabaseExist];
//    if (static_Logcashing_flag == YES) {
//        
//        NSArray *arrayObj = [[NSUserDefaults standardUserDefaults] objectForKey:@"log_array"];
//        return arrayObj;
//    }
//    return nil;
//}
//
/////***********************************Database coding*************************
//
//+(void) CheckDatabaseExist
//{
//    BOOL success = [Data CheckDatabaseExist];
//    if (!success) {
//        Data *data = [[Data alloc] init];
//        success = [data createDatabaseIfNotExist];
//        DLog(@"createDatabaseIfNotExist %d",success);
//    }
//    
//}
/////*********************************End Database coding***********************
//


//////////////////////////////////   jboloChat  ///////////////////////////////////////////////

+ (NSDictionary *) dict_withimage:(NSString *) jsonstring Imaagedata :(NSData *)imagedata static_URL :(NSString *)URL fileType:(NSString *)fileType
{
    NSDictionary *responseData = [NSDictionary dictionaryWithObjectsAndKeys:@"Connection Error.\n Please check internet..",@"message",@"25",@"code",nil];
    @try {
        NSMutableData *postBody = [NSMutableData data];
        NSString *stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
        
        //string to data.
        NSLog(@"jsonstring:-%@",jsonstring);
        
        //string to data.
        const char *datatoUpload = [jsonstring UTF8String];
        NSData *requestData = [NSData dataWithBytes: datatoUpload length: strlen(datatoUpload)];
        NSLog(@"jsonstring:-%@",[[NSString alloc] initWithData:requestData encoding:NSUTF8StringEncoding]);
        
        // add body to post
        [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[@"Content-Disposition: form-data; name=\"reqObject\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding ]];
        
        [postBody appendData:requestData];
        //[postBody appendData:[comment dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        /////////////****************************image.
        
        //image
        
        stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
        [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"item.jpg\"\r\n",fileType] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [postBody appendData:[@"Content-Type: image/jpeg\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        // get the image data from main bundle directly into NSData object
        //    NSData *imgData = UIImageJPEGRepresentation(item, 1.0);
        // add it to body
        [postBody appendData:imagedata];
        [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        // final boundary
        [postBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        ////////////*******************************end image.
        
        
        //encrypt data.
        //requestData = [Encrypt_Decrypt AES128EncryptWithKey:requestData];
        
        //request send code.
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: URL]];
        
        // set Content-Type in HTTP header
        stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", stringBoundary];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        //post request and data.
        //NSString *postLength = [NSString stringWithFormat:@"%d", [requestData length]];
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[postBody length]];
        [request setHTTPMethod: @"POST"];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        //[request setHTTPBody: requestData];
        [request setHTTPBody: postBody];
        [request setTimeoutInterval:600];
        
        
        NSError *e = nil;
        
//        double starttime = [[NSDate date] timeIntervalSince1970];
        NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&e];
//        double endtime = [[NSDate date] timeIntervalSince1970];
//        
//        double timeDiffInSecs =  endtime - starttime;
        
        if (returnData != nil) {
            
            //connection done and get data.
            
            DLog(@"Http Response:\n%@",[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]);
            NSError *localError;

            if(localError!=nil){
                responseData = [NSJSONSerialization JSONObjectWithData:returnData options:0 error:&localError];
                return responseData;
            }
            
        }
    }
    @catch (NSException *exception) {
        DLog(@"Exception :%@",exception);
    }
    @finally {
        
    }
    
    return responseData;
}
-(void)saveLogWithDict:(NSMutableDictionary*)resultsDictionary1 starttime:(float)starttime endtime:(float)endtime timeDiffInSecs:(float)timeDiffInSecs{
    @try {
        //log dictionary set.
        
        NSMutableDictionary *dict_log = [[NSMutableDictionary alloc] init];
        
        [dict_log setObject:@"test strig" forKey:@"req_url_json_string"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",starttime] forKey:@"req_start_timestamp"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",endtime] forKey:@"req_end_timestamp"];
        [dict_log setObject:[NSString stringWithFormat:@"%f",timeDiffInSecs] forKey:@"req_diff_timestamp"];
        
        NSString *strtmptest = [NSString stringWithFormat:@"%@",[resultsDictionary1 objectForKey:@"code"]];
        strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        
        if (strtmptest == (id)[NSNull null] || strtmptest.length < 1 || [strtmptest isEqualToString:@"(null)"])
        {
            
            [dict_log setObject:@"500" forKey:@"req_code"];
        }
        else{
            
            strtmptest = [strtmptest stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
            if (strtmptest.length > 0) {
                
                [dict_log setObject:[resultsDictionary1 objectForKey:@"code"] forKey:@"req_code"];
            }
            else {
                
                [dict_log setObject:@"500" forKey:@"req_code"];
            }
        }
        
        if (static_Logcashing_flag == YES) {
            
            NSArray *arrayObj = [[NSUserDefaults standardUserDefaults] objectForKey:@"log_array"];
            NSMutableArray *arrtemplog;
            if ([arrayObj count] > 0) {
                
                arrtemplog = [[NSMutableArray alloc] initWithArray:arrayObj];
                [arrtemplog addObject:dict_log];
                
            }
            else {
                
                arrtemplog = [[NSMutableArray alloc] init];
                [arrtemplog addObject:dict_log];
            }
            [[NSUserDefaults standardUserDefaults] setObject:arrtemplog forKey:@"log_array"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        } else {
            
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"log_array"];
        }
        
        
        //log dictionary set end.
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

+(NSDictionary *) Reveel_dict_withVideo:(NSString *) jsonstring Videodata :(NSData *)Videodata  videoname:(NSString *) videoname videoImageData:(NSData *)videoImageData imageData:(NSData *)imageData
{
    
    NSMutableData *postBody = [NSMutableData data];
    NSString *stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    NSLog(@"jsonstring:-%@",jsonstring);
    //string to data.
    const char *datatoUpload = [jsonstring UTF8String];
    
    NSData *requestData = [NSData dataWithBytes: datatoUpload length: strlen(datatoUpload)];
    
    [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [postBody appendData:[@"Content-Disposition: form-data; name=\"reqObject\"\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding ]];
    
    [postBody appendData:requestData];
    if (Videodata!=nil && videoImageData!=nil) {
#pragma data for video uploading start------------------------------------------------------------------
        
        
        
        [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
        [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [postBody appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"video\"; filename=\"%@.%@\"\r\n",@"Video",@"MOV"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [postBody appendData:[@"Content-Type: video/quicktime\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        //    [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [postBody appendData:Videodata];
        
        [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
#pragma data for video uploading complete------------------------------------------------------------------
        
#pragma data for VIDEOIMAGE uploading start------------------------------------------------------------------
        
        stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
        [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[@"Content-Disposition: form-data; name=\"videoimage\"; filename=\"item.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[@"Content-Type: image/jpeg\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        // get the image data from main bundle directly into NSData object
        //    NSData *imgData = UIImageJPEGRepresentation(item, 1.0);
        // add it to body
        [postBody appendData:videoImageData];
        [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
#pragma data for VIDEOIMAGE uploading Complete------------------------------------------------------------------
    }
    
#pragma data for Image uploading start------------------------------------------------------------------
    //    if (imageData!=nil ){
    //        stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    //        [postBody appendData:[[NSString stringWithFormat:@"--%@\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    //        [postBody appendData:[@"Content-Disposition: form-data; name=\"image\"; filename=\"item.jpg\"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    //        [postBody appendData:[@"Content-Type: image/jpeg\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    //        [postBody appendData:[@"Content-Transfer-Encoding: binary\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    //
    //        // get the image data from main bundle directly into NSData object
    //        //    NSData *imgData = UIImageJPEGRepresentation(item, 1.0);
    //        // add it to body
    //        [postBody appendData:imageData];
    //        [postBody appendData:[@"\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    //    }
#pragma data for Image uploading Complete------------------------------------------------------------------
    
    
    [postBody appendData:[[NSString stringWithFormat:@"--%@--\r\n", stringBoundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL: [NSURL URLWithString: [ApplicationConfiguration sharedInstance].SERVER_URL]];
    DLog(@" request url : %@",[ApplicationConfiguration sharedInstance].SERVER_URL);
    stringBoundary = @"0xKhTmLbOuNdArY---This_Is_ThE_BoUnDaRyy---pqo";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", stringBoundary];
    
    //[request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    NSString *postLength = [NSString stringWithFormat:@"%d", [postBody length]];
    [request setHTTPMethod: @"POST"];
    
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    [request setHTTPBody: postBody];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    [request setTimeoutInterval:600];
    
    
    NSError *e = nil;
    
    NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse: nil error:&e];
    
    
    if (returnData != nil)
    {
        
        
        //connection done and get data.
        
        DLog(@"Http Response:\n%@",[[NSString alloc] initWithData:returnData encoding:NSUTF8StringEncoding]);
        NSError *localError;
        
        if(localError==nil){
            NSDictionary *responseData1 = [NSJSONSerialization JSONObjectWithData:returnData options:0 error:&localError];
            return responseData1;
        }
        
    }
    return nil;
}

@end
