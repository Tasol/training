//
//  TableSectionArray.m
//  iJoomer
//
//  Created by tasol on 1/8/14.
//
//

#import "TableSectionArray.h"


@implementation TableSectionArray
@synthesize sectionArray;
@synthesize shortedSectionArray;

-(id)init
{
	if(self = [super init]) {
        self.sectionArray = [[NSMutableArray alloc] init];
        self.shortedSectionArray = [[NSMutableArray alloc] init];
	}
	return self;
}

-(BOOL)addObjectToSection:(TableRowObject*)object{
    @try {
        for (int i=0; i<self.sectionArray.count; i++) {
            TableSection *section = [self.sectionArray objectAtIndex:i];
            if([object.value.uppercaseString rangeOfString:section.name].location==0){
                [section addObject:object];
                return true;
            }
        }
        TableSection *section = [[TableSection alloc] init];
        section.name=[[object.value substringToIndex:1] uppercaseString];
        [section addObject:object];
        [self.sectionArray addObject:section];
        [self shotArray];
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    @finally {
        
    }
    return true;    
}

-(int)getcount{
//     NSLog(@"shortedself.sectionArray count :%d",shortedself.sectionArray.count);
//    NSLog(@"self.sectionArray count :%lu",(unsigned long)self.sectionArray.count);
//    return shortedself.sectionArray.count;
    return (int)self.shortedSectionArray.count;
}

-(TableSection*)sectionAtIndex:(int) index{
//    return [shortedself.sectionArray objectAtIndex:index];
    return [self.shortedSectionArray objectAtIndex:index];
}
-(void)shotArray{
    @try {
        
        NSLog(@"shotArray self.sectionArray :%lu",(unsigned long)self.sectionArray.count);
        self.shortedSectionArray =[[NSMutableArray alloc] initWithArray:
        
        [[self.sectionArray sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
            NSString *first = ((TableSection*)a).name;
            NSString *second = ((TableSection*)b).name;
            return [first compare:second];
        }] mutableCopy]];
        
        NSLog(@"shotArray self.shortedSectionArray:%lu",(unsigned long)self.shortedSectionArray.count);
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    @finally {
        
    }

}

-(TableSectionArray*)getSearchedArrayContains:(NSString*) text :(TableSectionArray*) tempSectionArray123{
    TableSectionArray *tempSectionArray = [[TableSectionArray alloc] init];
    @try {

        
        
        for (int i=0; i<self.sectionArray.count; i++) {
            TableSection *section = [self.sectionArray objectAtIndex:i];
            TableSection *tempSection = Nil;
            
            for (int j=0; j<[section getCount]; j++) {
                TableRowObject *rowObject = [section objectAtIndex:j];
                if([rowObject.value rangeOfString:text].location!=NSNotFound){
                    if (tempSection==Nil) {
                        tempSection = [[TableSection alloc] init];
                        tempSection.name=section.name;
                    }
                    [tempSectionArray addObjectToSection:rowObject];
                }
            }
        }
    }
    @catch (NSException *exception) {
        NSLog(@"%@",exception);
    }
    @finally {
        
    }
    
    return tempSectionArray;
}

-(void)removeAllObjects{
    @try {
        [self.sectionArray removeAllObjects];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}


@end
