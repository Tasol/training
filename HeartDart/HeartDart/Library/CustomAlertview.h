//
//  CustomAlertview.h
//  HeartDart
//
//  Created by Tasol on 9/2/14.
//  Copyright (c) 2014 HeartDart. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomAlertview : UIView
{

    UIView *viewAlertBG;
    UIImageView *imgAlertBG;
    UIImageView *imgSepratorLine;
    UIButton *btnCancel;
    UIButton *btnConfirm;
    UILabel *lblAlertTitle;
    UILabel *lblAlertMessage;
}
@property(nonatomic,retain)UIButton *btnCancel;
@property(nonatomic,retain)UIButton *btnConfirm;

-(void)setTheme:(NSString*)title AlertMessge:(NSString*)alertMessage;


@end
