//
//  HeartDartDataProvider.m
//  HeartDart
//
//  Created by tasol on 10/30/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "HeartDartDataProvider.h"
#import "RequestObjectProvider.h"
#import "Core_joomer.h"
#import "ErrorHandler.h"
#import "iJoomerDataProvider.h"

@implementation HeartDartDataProvider
@synthesize arrFriendRequest,stringMessage;
static HeartDartDataProvider *dataProvider=nil;


+ (HeartDartDataProvider*)sharedInstance
{
    if (dataProvider == nil)
    {
        dataProvider = [[super allocWithZone:NULL] init];
    }
    return dataProvider;
}


-(BOOL)registerUserWithTaskData:(NSDictionary*)taskData imageData:(NSData*)imageData
{
    /*
     {
     "task": "registration",
     "taskData": {
     "phone": "+919033366241",
     "username": "john",
     "fb": ''0'',
     "email": "john@gmail.com",
     "password": "123456",
     "deviceType": "android",
     "devicetoken": "911239255185854",
     "latitude": "23.0131062",
     "longitude": "72.5191458",
     }
     }Note : image should be in image tag
     */
    
    @try {
        NSString *jsonRequest =[RequestObjectProvider generateRequestObjectForLogin:@"registration" withTaskData:taskData];
        NSDictionary *responseData;
        
        if (imageData)
        {
            responseData = [Core_joomer sendRequest:jsonRequest Imaagedata:imageData];
        }
        else
        {
            responseData = [Core_joomer sendRequest:jsonRequest];
        }
        
        if(![ErrorHandler hasError:responseData])
        {
            NSLog(@"Registration response : %@",responseData);
            
            // Clears previous user Login Data
            
            NSUserDefaults *appDefaults = [NSUserDefaults standardUserDefaults];
            [appDefaults removeObjectForKey:@"email"];
            [appDefaults removeObjectForKey:@"profileDetails"];
            [appDefaults removeObjectForKey:@"password"];
            [appDefaults removeObjectForKey:@"isPremiumUser"];
            [appDefaults synchronize];
            [ApplicationConfiguration sharedInstance].isLogedIn=FALSE;
            
            return YES;
        }
        else
        {
            return NO;
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

-(BOOL)loginUser:(NSDictionary *)taskData
{
    @try
    {
        iJoomerDataProvider *controller = [[iJoomerDataProvider alloc]init];
        
        if([controller loginUserWithTaskData:taskData])
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
        return YES;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

-(NSDictionary *)getProfile:(NSDictionary *)taskData
{
    /*
     {
     "extName": "heartdart",
     "extView": "users",
     "extTask": "getProfile",
     "taskData": {
     "userID": "19"
     }
     }
     */
    
    NSDictionary *userDetails;
    @try
    {
        
        NSString *jsonString=[RequestObjectProvider generateRequestObjectFor:@"getProfile" withTaskData:taskData extView:@"users"];
        NSDictionary *responseData=[Core_joomer sendRequest:jsonString];
        
        if ([[responseData valueForKey:@"code"] integerValue]==200)
        {
            userDetails=[responseData valueForKey:@"userDetail"];
            
        }
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
    return userDetails;
}

-(BOOL)updatedProfile:(NSDictionary *)taskData
{
    
    /*
     {
     "extName": "heartdart",
     "extView": "users",
     "extTask": "updateProfile",
     "taskData": {
     "email": "nilesh@tasolglobal.com",
     "userName": "nils",
     "password": "123",
     "moodColor": "#aabbcc"
     }
     }
     */
    @try
    {
        NSString *jsonString=[RequestObjectProvider generateRequestObjectFor:@"updateProfile" withTaskData:taskData extView:@"users"];
        NSDictionary *responseData=[Core_joomer sendRequest:jsonString];
        if ([[responseData valueForKey:@"code"] integerValue]==200)
        {
            
        }
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    return TRUE;
}

-(NSMutableArray * )getUserList:(NSDictionary *)taskData
{
   /* {
        "extName": "heartdart",
        "extView": "users",
        "extTask": "getUserList",
        "taskData": {
            "friendsOnly": "0/1"
        }
    }*/

    NSMutableArray *arrFriend=[[NSMutableArray alloc]init];
    arrFriendRequest=[[NSMutableArray alloc]init];
    @try
    {
        NSString *jsonString=[RequestObjectProvider generateRequestObjectFor:@"getUserList" withTaskData:taskData extView:@"users"];
        NSDictionary *responseData=[Core_joomer sendRequest:jsonString];
        if ([[responseData valueForKey:@"code"]integerValue]==200)
        {
            arrFriend=[responseData valueForKey:@"firends"];
           arrFriendRequest=[responseData valueForKey:@"friendRequest"];
        }
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    return arrFriend;
    

}


-(BOOL)sendFriendRequest:(NSDictionary *)taskData
{
    /*   "extName": "heartdart",
     "extView": "users",
     "extTask": "sendFriendRequest",
     "taskData": {
     "userID": "20"
     }
     }
*/
    @try
    {
        stringMessage=[[NSString alloc]init];
        NSString *jsonString=[RequestObjectProvider generateRequestObjectFor:@"sendFriendRequest" withTaskData:taskData extView:@"users"];
        
        NSDictionary *responseData=[Core_joomer sendRequest:jsonString];
        
        if ([[responseData valueForKey:@"code"] integerValue]==200)
        {
            stringMessage=[[responseData valueForKey:@"pushNotificationData"] valueForKey:@"message"];
        }

        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
        return TRUE;
}

-(void)responseToFriendRequest:(NSDictionary *)taskData
{
    @try
    {
       /* {
            "extName": "heartdart",
            "extView": "users",
            "extTask": "responseToFriendRequest",
            "taskData": {
                "userID": "number",
                "response": "accept/decline"
            }
        }*/

        NSString *jsonString=[RequestObjectProvider generateRequestObjectFor:@"responseToFriendRequest" withTaskData:taskData extView:@"users"];
        NSDictionary *responseData=[Core_joomer sendRequest:jsonString];
        if ([[responseData valueForKey:@"code"] integerValue]==200)
        {
            
        }
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}
@end
