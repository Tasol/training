//
//  ProgressBar.h
//  LGOM
//
//  Created by Kalpesh on 9/15/14.
//  Copyright (c) 2014 LGOM. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ResultBar : UIView
{
    CGFloat startAngle;
    CGFloat endAngle;
    long timeRemaining;
    NSString *strTime;
    NSString *strVote;
    
    int yesVote;
    int noVote;
    int totalVote;
}
@property(nonatomic,assign)int totalInvitation;
@property(nonatomic,assign)int collectedVote;

@property(nonatomic,assign)int yesVote;
@property(nonatomic,assign)int noVote;
@property(nonatomic,assign)int totalVote;

@property(nonatomic,assign)float percent;
@property(nonatomic,assign)long timeRemaining;

@property(nonatomic,retain)NSString *strTime;
@property(nonatomic,retain)NSString *strVote;

@end
