//
//  RequestObjectProvider.m
//  Intafy
//
//  Created by tasol on 2/24/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "RequestObjectProvider.h"
#import "NSUtil.h"

@implementation RequestObjectProvider
+(NSString*)generateRequestObjectForLogin:(NSString*)extTask withTaskData:(NSDictionary*)taskData {
    NSDictionary *dictTask = [NSDictionary dictionaryWithObjectsAndKeys:extTask, @"task", taskData, @"taskData", nil];
    return [dictTask JSONString];
}

+(NSString*)generateRequestObjectFor:(NSString*)extTask withTaskData:(NSDictionary*)taskData extView:(NSString*)extView{
    NSDictionary *dictTask = [NSDictionary dictionaryWithObjectsAndKeys:@"heartdart",@"extName",extTask, @"extTask", taskData, @"taskData",extView,@"extView", nil];
    return [dictTask JSONString];
}

+(NSString*)generateRequestObjectForMessageWith:(NSString*)extTask withTaskData:(NSDictionary*)taskData extView:(NSString*)extView{
    NSDictionary *dictTask = [NSDictionary dictionaryWithObjectsAndKeys:@"message",@"extName",extTask, @"extTask", taskData, @"taskData",extView,@"extView", nil];
    return [dictTask JSONString];
}
@end
