//
//  CustomAnnotationview.m
//  LGOM
//
//  Created by tasol on 9/9/14.
//  Copyright (c) 2014 LGOM. All rights reserved.
//

#import "CustomAnnotationview.h"

@implementation CustomAnnotationview
@synthesize coordinate;

-(id)initWith:(CLLocationCoordinate2D)coordinates friendsDetail:(NSDictionary *)friendsDetails image:(UIImage*)image
{
    @try
    {
        coord = coordinates;
        friendsDetail = friendsDetails;
        imgProfile =image;
        return self;
    }
    @catch (NSException *exception)
    {
        
    }
    @finally {
        
    }
}

-(NSDictionary *)getFriendsDetail
{
    @try
    {
        return friendsDetail;
    }
    @catch (NSException *exception)
    {
    }
    @finally {
        
    }
}
- (CLLocationCoordinate2D)coordinate;
{
    return coord;
}
-(UIImage *)getImage
{
    return imgProfile;
}


@end
