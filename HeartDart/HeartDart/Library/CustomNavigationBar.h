//
//  CustomNavigationBar.h
//  Intafy
//
//  Created by tasol on 2/13/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomNavigationBar : UIView
{
    UIImageView *imgNavigationBG;
    UILabel *lblTitle;
    UIImageView *imgBottomLine;
    UIButton *btnRightSideButton;
    UIButton *btnLeftSideButton;
}
@property(nonatomic,retain)UILabel *lblTitle;
@property(nonatomic,retain) UIButton *btnRightSideButton;
@property(nonatomic,retain) UIButton *btnLeftSideButton;
-(void)setTitle:(NSString*)title;
-(void)setRightSideButtonText:(NSString*)text withTargate:(id)target action:(SEL)selector forEvent:(UIControlEvents)event;
-(void)setLeftSideButtonText:(NSString*)text withTargate:(id)target action:(SEL)selector forEvent:(UIControlEvents)event;
-(void)deleteLeftSideButton;
-(void)deleteRightSideButton;
-(void)removeTargetFromRightSideButton:(id)target action:(SEL)selector forEvent:(UIControlEvents)event;
-(void)removeTargetFromLeftSideButton:(id)target action:(SEL)selector forEvent:(UIControlEvents)event;
-(void)setRightSideButtonImageFromURL:(NSString*)imageURL;
-(void)setRightSideButtonImage:(UIImage*)image;
-(void)setLeftSideButtonImage:(UIImage*)image;
@end
