//
//  ApplicationConfiguration.h
//  Intafy
//
//  Created by tasol on 2/11/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "GPSLocationListner.h"

@interface ApplicationConfiguration : NSObject


@property(nonatomic,assign)int modelNum;
@property(nonatomic,assign)BOOL isSocialStreamDownloading;
@property(nonatomic,retain)NSString *SERVER_URL;
@property(nonatomic,retain)NSString *POSTFIX_XIB;
@property(nonatomic,retain)NSDictionary *globalconfig;
@property(nonatomic,retain)NSDictionary *extentionconfig;
@property(nonatomic,retain)NSDictionary *versioninfo;
@property(nonatomic,retain)UIColor *buttonBorderColor;
@property(nonatomic,retain)UIColor *textColor;
@property(nonatomic)UIFont *descriptionFont;
@property(nonatomic,assign)float systemVersion;
@property(nonatomic,retain)NSString *selectedNetworkID;
@property(nonatomic,retain)NSString *selectedNetworkUserID;
@property(nonatomic,retain)NSString *deviceToken;
@property  (nonatomic,assign) int appType;
@property (nonatomic,assign) BOOL isLogedIn;
@property (nonatomic,retain) NSString *restID;
@property (nonatomic,retain) NSString *userID;

@property(nonatomic, retain) GPSLocationListner *locationManager;
+ (ApplicationConfiguration*)sharedInstance;
-(BOOL)fetchApplicationConfig;
-(void)genarateServerURL;
-(void)genarateServerURLWithNetworkId;
-(void)genarateServerURLDynamic:(NSString*)URL;

-(UIFont*)getFontWithHeight:(int)size;
-(void)createDatabase;
@end
