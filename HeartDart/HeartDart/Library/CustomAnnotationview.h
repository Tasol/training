//
//  CustomAnnotationview.h
//  LGOM
//
//  Created by tasol on 9/9/14.
//  Copyright (c) 2014 LGOM. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@interface CustomAnnotationview : NSObject<MKAnnotation>
{
    CLLocationCoordinate2D coord;
    NSDictionary *friendsDetail;
    UIImage *imgProfile;

}
-(UIImage*)getImage;
-(id)initWith:(CLLocationCoordinate2D)coordinates friendsDetail:(NSDictionary*)friendsDetails image:(UIImage*)image;
-(NSDictionary*)getFriendsDetail;
@end
