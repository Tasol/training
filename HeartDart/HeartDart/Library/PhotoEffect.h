//
//  PhotoEffect.h
//  LGOM
//
//  Created by Kalpesh on 9/24/14.
//  Copyright (c) 2014 LGOM. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhotoEffect : NSObject
@property(nonatomic,strong)NSArray *effectArray;
+ (UIImage *) generateMonoImage: (UIImage *)icon withColor:(UIColor *)color;
+(UIImage*)getImageWithEffect:(NSData*)beginImage withEffect:(NSString*)Effect;
+(NSMutableArray*)getImages:(NSData*)image effects:(NSArray*)effectArray;
@end
