//
//  TableSectionArray.h
//  iJoomer
//
//  Created by tasol on 1/8/14.
//
//

#import <Foundation/Foundation.h>
#import "TableRowObject.h"
#import "TableSection.h"

@interface TableSectionArray : NSObject{

    NSMutableArray *sectionArray;
    NSMutableArray *shortedSectionArray;
}
@property(nonatomic,retain)NSMutableArray *sectionArray;
@property(nonatomic,retain)NSMutableArray *shortedSectionArray;

-(BOOL)addObjectToSection:(TableRowObject*)object;
-(int)getcount;
-(TableSection*)sectionAtIndex:(int) index;
-(TableSectionArray*)getSearchedArrayContains:(NSString*) text :(TableSectionArray*) tempSectionArray123;
-(void)removeAllObjects;
@end
