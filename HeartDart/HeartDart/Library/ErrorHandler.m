//
//  ErrorHangler.m
//  Kalpesh tita
//
//  Created by tasol on 10/8/13.
//
//

#import "ErrorHandler.h"
@implementation ErrorHandler

static NSString *message=@"";
+ (BOOL)hasError:(NSDictionary*)dict{
    int errorCode = (int)[[dict valueForKey:@"code"] integerValue];
    
    message=[dict valueForKey:@"message"];
    BOOL returnflag = false;
	switch (errorCode) {
		case jBadRequest:
            [self performSelectorOnMainThread:@selector(jBadRequest) withObject:nil waitUntilDone:NO];
            returnflag =true;
			break;
            
		case jLoginRequired:
            returnflag =  true;
			break;
            
		case jErroronServer:
            
            [self performSelectorOnMainThread:@selector(jErrorMessage) withObject:nil waitUntilDone:NO];
            returnflag =  true;
			break;
            
        case jNoContent:
            
            [self performSelectorOnMainThread:@selector(jNoContent) withObject:nil waitUntilDone:NO];
            returnflag =  true;
			break;
            
        case jErrorMessage:
            
            [self performSelectorOnMainThread:@selector(jErrorMessage) withObject:nil waitUntilDone:NO];
            returnflag =  true;
			break;
            
        case jUnsupportedFile:
            
            [self performSelectorOnMainThread:@selector(jUnsupportedFile) withObject:nil waitUntilDone:NO];
            returnflag =  true;
			break;
            
        case jInvalidData:
            
            [self performSelectorOnMainThread:@selector(jInvalidData) withObject:nil waitUntilDone:NO];
            returnflag =  true;
			break;
            
        case jUserNameError:
            
            [self performSelectorOnMainThread:@selector(jUserNameError) withObject:nil waitUntilDone:NO];
            returnflag =  true;
			break;
            
        case jEmailError:
            
            [self performSelectorOnMainThread:@selector(jEmailError) withObject:nil waitUntilDone:NO];
            returnflag =  true;
			break;
            
        case jFBOption:
            
            [self performSelectorOnMainThread:@selector(jFBOption) withObject:nil waitUntilDone:NO];
            returnflag =  true;
			break;
            
        case jSessionExpire:
            returnflag =  true;
			break;
            
        case jReportedContent:
            
            [self performSelectorOnMainThread:@selector(jReportedContent) withObject:nil waitUntilDone:NO];
            returnflag =  true;
			break;
            
        case JPermissionError:
            
            [self performSelectorOnMainThread:@selector(JPermissionError) withObject:nil waitUntilDone:NO];
            returnflag =  true;
			break;
            
        case jDuplicateData:
            
            [self performSelectorOnMainThread:@selector(jDuplicateData) withObject:nil waitUntilDone:NO];
            returnflag =  true;
			break;
            
        case jWaitingForPermission:
            
            [self performSelectorOnMainThread:@selector(jWaitingForPermission) withObject:nil waitUntilDone:NO];
            returnflag =  true;
			break;
        case jSuccess:
            returnflag =  false;
			break;

		default:
            [self performSelectorOnMainThread:@selector(jBadData) withObject:nil waitUntilDone:NO];
            returnflag =  true;
			break;
	}
    return returnflag;
}

+(void)jBadData{
    [self showAlert:NSLocalizedString(@"",@"Error title") Content:NSLocalizedString(@"ALERT_WRONG_DATA", @"")];
}

+(void)jBadRequest{
    [self showAlert:NSLocalizedString(@"",@"Error title") Content:NSLocalizedString(@"ALERT_WRONG_DATA", @"")];
}

+(void)jErroronServer{
    [self showAlert:NSLocalizedString(@"",@"Error title") Content:NSLocalizedString(@"ALERT_SERVER_ERROR", @"")];
}

+(void)jErrorMessage{
    [self showAlert:NSLocalizedString(@"",@"Error title") Content:NSLocalizedString(@"ALERT_NO_REQUEST", @"")];
}
+(void)jNoContent{
    //[self showAlert:NSLocalizedString(@"",@"Error title") Content:NSLocalizedString(@"ALERT_NO_CONTENT", @"")];
}
+(void)jUnsupportedFile {
    [self showAlert:NSLocalizedString(@"",@"Error title") Content:NSLocalizedString(@"ALERT_UNSUPPORTERTED_FILE", @"")];
}
+(void)jInvalidData {
    [self showAlert:NSLocalizedString(@"",@"Error title") Content:NSLocalizedString(@"ALERT_LIMIT_EXCEED", @"")];
}

+(void)jUserNameError {
    [self showAlert:NSLocalizedString(@"",@"Error title") Content:NSLocalizedString(@"ALERT_USERNAME_EXISTS", @"")];
}

+(void)jEmailError {
    [self showAlert:NSLocalizedString(@"",@"Error title") Content:NSLocalizedString(@"ALERT_EMAIL_EXISTS", @"")];
}

+(void)jFBOption {
    [self showAlert:NSLocalizedString(@"",@"Error title") Content:NSLocalizedString(@"ALERT_NO_FACEBOOK_USER", @"")];
}

+(void)jReportedContent {
    [self showAlert:NSLocalizedString(@"",@"Error title") Content:NSLocalizedString(@"ALERT_PERMISSION_DENIED", @"")];
}

+(void)JPermissionError {
    [self showAlert:NSLocalizedString(@"",@"Error title") Content:NSLocalizedString(@"ALERT_RESTRICTED_ACCESS", @"")];
}

+(void)jDuplicateData {
    [self showAlert:NSLocalizedString(@"",@"Error title") Content:NSLocalizedString(@"ALERT_REWUEST_EXIST", @"")];
}

+(void)jWaitingForPermission {
    [self showAlert:NSLocalizedString(@"",@"Error title") Content:NSLocalizedString(@"ALERT_AWAITING_APPROVAL", @"")];
}

+ (void)showAlert:(NSString *)title Content:(NSString *)bodyText {
    
    UIAlertView *alert;
    
    @try {
        if ((![message isKindOfClass:[NSNull class]]&& message!=nil ) &&![message isEqualToString:@""]) {
            alert=[[UIAlertView alloc] initWithTitle:title message:message
                                            delegate:self cancelButtonTitle:NSLocalizedString(@"TITLE_CANCEL",@"") otherButtonTitles: nil];
            

//            AlertViewPopUP *alertPopUp=[[AlertViewPopUP alloc]initWIthTarget:self andTitle:title andMessage:message];
        
            message=@"";
            
//            [[UIApplication sharedApplication].keyWindow addSubview:alertPopUp];
        
        }
        else
        {
            alert=[[UIAlertView alloc] initWithTitle:title message:bodyText
                                            delegate:self cancelButtonTitle:NSLocalizedString(@"TITLE_CANCEL",@"") otherButtonTitles: nil];
//            AlertViewPopUP *alertPopUp=[[AlertViewPopUP alloc]initWIthTarget:self andTitle:title andMessage:message];
//            
//    
//            
//            [[UIApplication sharedApplication].keyWindow addSubview:alertPopUp];
            
    

        }
        
        [alert setContentMode:UIViewContentModeScaleAspectFit];
        [alert show];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

@end
