//
//  iJoomerDataProvider.m
//  Intafy
//
//  Created by tasol on 2/22/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "iJoomerDataProvider.h"
#import "RequestObjectProvider.h"
#import "Core_joomer.h"
#import "NSUtil.h"
#import "DataBase.h"

@implementation iJoomerDataProvider

-(void)storeDataINDataBase :(NSDictionary *)dict
{
    @try {
        NSArray *tableKeys = [dict allKeys];
        for (NSString *key in tableKeys)
        {
           if([[dict objectForKey:key] isKindOfClass:[NSArray class]])
           {
               NSArray *rowList = [dict objectForKey:key];
               
               if ([rowList isKindOfClass:[NSArray class]])
               {
                   NSArray *arrallkey;
                   NSArray *values;
                   NSDictionary *datadict;
                   if ([rowList count] > 0)
                   {
                       int p=0;
                       int maxKeycount = (int)[[[rowList objectAtIndex:0] allKeys] count];
                       int index = 0;
                       
                       for (int i=0; i<rowList.count; i++)
                       {
                           p = (int)[[[rowList objectAtIndex:i] allKeys] count];
                           
                           if (p > maxKeycount)
                           {
                               maxKeycount = p;
                               index = i;
                           }
                       }
                       
                       datadict = [rowList objectAtIndex:index];
                       arrallkey = [datadict allKeys];
                       values = [datadict allValues];
                       
                    }
                   for (int i = 0; i < [rowList count]; i++)
                   {
                       datadict = [rowList objectAtIndex:i];
                       values = [datadict allValues];
                       NSArray *allKeys = [datadict allKeys];
                       [self insertDataWithColumn:allKeys values:values forTable:key];
                   }
               }
           
            }
           else if ([[dict objectForKey:key] isKindOfClass:[NSDictionary class]])
           {
               

               NSDictionary *object =[dict objectForKey:key];
               NSArray *arrallkey = [object allKeys];
               NSArray *values = [object allValues];

              
                   for (NSString *columName in arrallkey) {
                       if(![[DataBase sharedInstance] checkColum:columName inTable:[NSString stringWithFormat:@"%@%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"],key]]){
                           [[DataBase sharedInstance] createColum:columName inTable:[NSString stringWithFormat:@"%@%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"],key]];
                       
                   }
               }
               
               [self insertDataWithColumn:arrallkey values:values forTable:key];
               
           }
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
-(void)insertDataWithColumn:(NSArray*)colums values:(NSArray*)values forTable:(NSString*)table{
    @try {
        
        NSString *strcolumenname = [[NSString alloc] init];
        NSString *strinsertvalues = [[NSString alloc] init];
        for (int l = 0 ; l < [colums count]; l++)
        {
            
            if((![[values objectAtIndex:l] isKindOfClass:[NSArray class]]&&![[values objectAtIndex:l] isKindOfClass:[NSDictionary class]])||
               [((NSString*)[colums objectAtIndex:l]).lowercaseString isEqualToString:@"fields"]){
                NSString* Strings;
                    strcolumenname = [NSString stringWithFormat:@"%@%@,",strcolumenname,[colums objectAtIndex:l]];

                if ([[values objectAtIndex:l] isKindOfClass:[NSArray class]] )
                {
                    Strings=[NSString stringWithFormat:@"%@",[((NSArray*)[values objectAtIndex:l]) JSONString]];
                }else if([[values objectAtIndex:l] isKindOfClass:[NSDictionary class]]){
                    Strings=[NSString stringWithFormat:@"%@",[((NSDictionary*)[values objectAtIndex:l]) JSONString]];
                }else{
                    Strings=[NSString stringWithFormat:@"%@",[values objectAtIndex:l]];
                }
                Strings=[Strings stringByReplacingOccurrencesOfString: @"\"" withString:@"\\'"];
                strinsertvalues = [NSString stringWithFormat:@"%@\"%@\",",strinsertvalues,Strings];
            }
        }

        
        NSString *QUR = [NSString stringWithFormat:@"INSERT OR REPLACE INTO \"%@\"(%@) VALUES(%@);",[NSString stringWithFormat:@"%@%@",[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"],table],[strcolumenname substringToIndex:strcolumenname.length-1], [strinsertvalues substringToIndex:strinsertvalues.length-1]];
        [[DataBase sharedInstance] insertData:QUR];
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

-(BOOL)registerUserWithTaskData:(NSDictionary*)taskData
{
    /*
     {
     "task": "registration",
     "taskData": {
     "phone": "+919033366241",
     "username": "john",
     "fb": ''0'',
     "email": "john@gmail.com",
     "password": "123456",
     "deviceType": "android",
     "devicetoken": "911239255185854",
     
     }
     }
     */
    @try
    {
        NSString *jsonRequest=[RequestObjectProvider generateRequestObjectForLogin:@"registration" withTaskData:taskData];
        NSDictionary *responseObject=[Core_joomer sendRequest:jsonRequest];
        
        if (![ErrorHandler hasError:responseObject]) {
            NSLog(@"Registration response : %@",responseObject);
            return YES;
        }
        else{
            
            return NO;
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}
-(BOOL)logoutUserWithTaskData:(NSDictionary *)taskData
{
    /*
     {"task":"logout","taskData":{"devicetoken":"sfaf","type":"iphone"}}
     */
    
    @try
    {
        NSString *jsonRequest=[RequestObjectProvider generateRequestObjectForLogin:@"logout" withTaskData:taskData];
        NSDictionary *responseData=[Core_joomer sendRequest:jsonRequest];
        
        if([[responseData valueForKey:@"code"] integerValue]==200 || [[responseData valueForKey:@"code"] integerValue]==400)
        {
            NSUserDefaults *appDefaults = [NSUserDefaults standardUserDefaults];
            
            [ApplicationConfiguration sharedInstance].isLogedIn=NO;
            [appDefaults setBool:NO forKey:@"isLoggedIn"];
            [appDefaults removeObjectForKey:@"fb"];
            [appDefaults removeObjectForKey:@"deviceToken"];
            [appDefaults synchronize];
            [self clearDataBase];
            return YES;
            
        }
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    return NO;
}

-(void)clearDataBase
{
    @try {
        NSString *query = @"SELECT 'DROP TABLE ' ||  name as deleteQuery FROM sqlite_master WHERE type='table';";
        NSMutableArray *array = [[DataBase sharedInstance] getDataFor:query];
        for (NSDictionary *dict in array) {
            [[DataBase sharedInstance] deleteData:[dict valueForKey:@"deleteQuery"]];
        }
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}


-(BOOL)loginUserWithTaskData:(NSDictionary*)taskData
{
    /*
     {
     "task": "login",
     "taskData": {
     "email": "john@gmail.com",
     "deviceType": "android",
     "devicetoken": "911239255185854",
     "longitude": "72.5191458",
     "password": "123456",
     "latitude": "23.0131062",
     "fb": "0"
     }
     }
     */
    @try
    {
        UIAlertView *alert;
        NSString *msg;
        NSMutableDictionary *task=[[NSMutableDictionary alloc]init];
        [task setValue:[taskData valueForKey:@"password"] forKey:@"password"];
        [task setValue:[taskData valueForKey:@"email"] forKey:@"email"];
        
//        [task setValue:[taskData valueForKey:@"fb"] forKey:@"fb"];
        

        [task setValue:[ApplicationConfiguration sharedInstance].deviceToken forKey:@"devicetoken"];

        [task setValue:[NSString stringWithFormat:@"%f",[ApplicationConfiguration sharedInstance].locationManager.latitude] forKey:@"latitude"];
        [task setValue:[NSString stringWithFormat:@"%f",[ApplicationConfiguration sharedInstance].locationManager.longitude] forKey:@"longitude"];
        [task setValue:@"iphone" forKey:@"deviceType"];
        
        NSString *jsonRequest=[RequestObjectProvider generateRequestObjectForLogin:@"login" withTaskData:task];
        long reqTimeStamp = [[NSDate date] timeIntervalSince1970];
        NSDictionary *responseData=[Core_joomer sendRequest:jsonRequest];
        long resTimeStamp = [[NSDate date] timeIntervalSince1970];
        
        if([[responseData valueForKey:@"code"] integerValue]==200)
        {
              [ApplicationConfiguration sharedInstance].restID=[[responseData valueForKey:@"userData"] valueForKey:@"userID"]; 
           
//            [[ApplicationConfiguration sharedInstance] userID]=[responseData valueForKey:@"userID"];
            NSUserDefaults *appDefaults = [NSUserDefaults standardUserDefaults];
            long timeStampToStore = [[NSDate date] timeIntervalSince1970] - [[responseData valueForKey:@"timestamp"] longLongValue];
            if (timeStampToStore<0) {
                timeStampToStore = timeStampToStore*(-1);
            }
            timeStampToStore = timeStampToStore+((resTimeStamp-reqTimeStamp)/2);
            
            [appDefaults setValue:[NSString stringWithFormat:@"%ld",timeStampToStore] forKey:TAG_TIMESTAMPDIFF];
//            [appDefaults synchronize];
            
            // Clears user info if user login is different then previous login
            
            if (![[appDefaults valueForKey:@"email"] isEqualToString:[taskData valueForKey:@"email"]])
            {
                [appDefaults removeObjectForKey:@"email"];
                [appDefaults removeObjectForKey:@"profileDetails"];
                [appDefaults removeObjectForKey:@"password"];
                [appDefaults removeObjectForKey:@"isPremiumUser"];
                
            }
            
//            if ([[taskData valueForKey:@"fb"] integerValue]==0)
//            {
//                [appDefaults setValue:@"0" forKey:@"fb"];
//            }
//            else
//            {
//                [appDefaults setValue:@"1" forKey:@"fb"];
//            }

            // Store Proile response for the user
            
            [appDefaults setObject:[responseData valueForKey:@"userData"] forKey:@"userBasicInfo"];
//            if ([[[responseData valueForKey:@"userData"] valueForKey:@"isPremiumUser"] integerValue]==1)
//            {
//                [appDefaults setBool:YES forKey:@"isPremiumUser"];
//            }
//            else
//            {
//                [appDefaults setBool:NO forKey:@"isPremiumUser"];
//            }
            [appDefaults setObject:[responseData valueForKey:@"settings"] forKey:@"Notificationsettings"];
            
            [appDefaults setObject:[taskData valueForKey:@"email"] forKey:@"email"];
            [appDefaults setObject:[taskData valueForKey:@"password"] forKey:@"password"];
            [appDefaults setBool:YES forKey:@"isLoggedIn"];
//            [appDefaults setObject:string forKey:@"userID"];
            
            [appDefaults synchronize];
         
            [ApplicationConfiguration sharedInstance].isLogedIn=YES;
            
            
        }
        else if ([[responseData valueForKey:@"code"] integerValue]==403)
        {
            msg = [responseData valueForKey:@"message"];
            alert=[[UIAlertView alloc]initWithTitle:@"" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        else if([[responseData valueForKey:@"code"] integerValue]==401)
        {
            msg=[responseData valueForKey:@"message"];
            alert=[[UIAlertView alloc]initWithTitle:msg message:@"Please enter valid email" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }
        else if ([[responseData valueForKey:@"code"] integerValue]==402)
        {
            msg=[responseData valueForKey:@"message"];
            alert=[[UIAlertView alloc]initWithTitle:msg message:@"Please enter valid password" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return NO;
        }else if (([[responseData valueForKey:@"code"] integerValue]==400 || [[responseData valueForKey:@"code"] integerValue]==400))
        {

            [[NSNotificationCenter defaultCenter] postNotificationName:@"loginWithFacebookFail" object:nil];
            return NO;
        }
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0ul), ^
                       {
                           [self getUserProfileFromServer];
                       });
        return YES;
    }
    @catch (NSException *exception)
    {
        NSLog(@"Excepation found while validateUserLoginFromServer :%@",exception);
    }
    @finally
    {
        
    }
    return NO;
}
-(BOOL)checkLogin
{
    NSUserDefaults *standardUserDefault=[NSUserDefaults standardUserDefaults];
    NSString *userName=[standardUserDefault valueForKey:@"email"];
    NSString *userPass=[standardUserDefault valueForKey:@"password"];
//    NSString *deviceToken = [standardUserDefault valueForKey:@"deviceToken"];
    BOOL isLoggedIn=[standardUserDefault boolForKey:@"isLoggedIn"];
    
    if(userName && userPass && isLoggedIn)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}
-(BOOL)validateLogin
{
    NSUserDefaults *standardUserDefault=[NSUserDefaults standardUserDefaults];
    
    NSString *userName=[standardUserDefault valueForKey:@"email"];
    NSString *userPass=[standardUserDefault valueForKey:@"password"];
    [ApplicationConfiguration sharedInstance].deviceToken = [standardUserDefault valueForKey:@"deviceToken"];
    NSMutableDictionary *taskData=[[NSMutableDictionary alloc]init];
    [taskData setValue:userName forKey:@"email"];
    [taskData setValue:userPass forKey:@"password"];
    
    int fb=(int)[[standardUserDefault valueForKey:@"fb"] integerValue];
    if (fb==0)
    {
         [taskData setValue:@"0" forKey:@"fb"];
    }
    else
    {
         [taskData setValue:@"1" forKey:@"fb"];
    }
    BOOL success=[self loginUserWithTaskData:taskData];
    if(success)
    {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0ul), ^
        {
        [self getUserProfileFromServer];
        });
        
        return YES;
    }
    else
    {
        return NO;
    }
}


-(NSDictionary *)getUserProfileFromServer
{
    /*
     {
     "extName": "heartdart",
     "extView": "users",
     "extTask": "getProfile",
     "taskData": {
     "userID": "19"
     }
     }
     */
    @try {
        NSDictionary *userDetails;
        NSMutableDictionary *postVariable=[[NSMutableDictionary alloc]init];
        
        NSString *jsonRequest=[RequestObjectProvider generateRequestObjectFor:@"getProfile" withTaskData:postVariable extView:@"users"];
        NSDictionary *responseData=[Core_joomer sendRequest:jsonRequest];
        
        if([[responseData valueForKey:@"code"] intValue]==200)
        {
            NSLog(@"Profile Field : %@",responseData);
           
            NSUserDefaults *appSettingDefaults = [NSUserDefaults standardUserDefaults];
            
            [appSettingDefaults setObject:[responseData valueForKey:@"userDetail"] forKey:@"UserProfileInfo"];
            
            
            [appSettingDefaults synchronize];
            
        }
        return userDetails;
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

+(BOOL)forgotLogin:(NSDictionary *)taskData
{
    /*
     {"task":"forgotPassword","taskData":{"email":""}}
     */
    @try
    {
        NSString *jsonRequest =[RequestObjectProvider generateRequestObjectForLogin:@"forgotPassword" withTaskData:taskData];
        
        NSDictionary *responseData = [Core_joomer sendRequest:jsonRequest];
        
        if([[responseData valueForKey:@"code"] integerValue]==200)
        {
                return TRUE;
        }
        else if([[responseData valueForKey:@"code"] integerValue]==500)
        {
                //[ErrorHandler hasError:responseData];
            UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Invalid Email address." message:@"" delegate:@"" cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            return FALSE;
        }
    }
    @catch (NSException *exception)
    {
    }
    @finally
    {
    }
    return FALSE;
}
#pragma for FORGOT LOGIN Process END--------------------------------------------------------------------------------

#pragma this scode is used for facebook

//-(BOOL)login
//{
//    @try {
//        
//        if(![self openSessionWithAllowLoginUI:NO])
//        {
//            return [self openSessionWithAllowLoginUI:YES];
//        }
//        return false;
//    }
//    @catch (NSException *exception) {
//        NSLog(@"Exception Found : %@",exception);
//    }
//    @finally {
//        
//    }
//}
//- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI {
//    
//    @try {
//        
//        
//        BOOL flag=[FBSession openActiveSessionWithReadPermissions:defaultPermissions
//                                                     allowLoginUI:allowLoginUI
//                                                completionHandler:^(FBSession *session,
//                                                                    FBSessionState state,
//                                                                    NSError *error) {
//                                                    fbSession=session;
//                                                    [self sessionStateChanged:session
//                                                                        state:state
//                                                                        error:error];
//                                                }];
//        return flag;
//    }
//    @catch (NSException *exception) {
//        NSLog(@"Exception Found : %@",exception);
//    }
//    @finally {
//        
//    }
//}
//- (void)sessionStateChanged:(FBSession *)session
//                      state:(FBSessionState) state
//                      error:(NSError *)error
//{
//    
//    @try{
//        switch (state) {
//            case FBSessionStateOpen:
//                if (!error) {
//                    // We have a valid session
//                    NSLog(@"User session open successfully");
//                    // NSLog(@"session %@",session);
//                }
//                break;
//            case FBSessionStateClosed:
//            {
//                NSLog(@"session closed");
//            }
//                break;
//            case FBSessionStateClosedLoginFailed:
//            {
//                [FBSession.activeSession closeAndClearTokenInformation];
//                NSLog(@"state closed and loginfailed");
//            }
//                break;
//            default:
//                break;
//        }
//        if (error)
//        {
//            UIAlertView *alertView = [[UIAlertView alloc]
//                                      initWithTitle:@"Error"
//                                      message:error.localizedDescription
//                                      delegate:nil
//                                      cancelButtonTitle:@"OK"
//                                      otherButtonTitles:nil];
//            [alertView show];
//        }
//    }
//    @catch (NSException *exception) {
//        NSLog(@"Exception Found : %@",exception);
//    }
//    @finally {
//        
//    }
//}

-(NSDictionary*)retrieveUserInfoFromFacebook
{
    @try{
//        if (!self.facebookObj) {
//            self.facebookObj = [[Facebook alloc]init];
//        }
        NSDictionary *userData = [[NSDictionary alloc] init];
//        NSDictionary *dictFields=[NSDictionary dictionaryWithObject:@"picture,id,birthday,email,name,gender,username,gender,first_name,last_name" forKey:@"fields"];
        NSDictionary *dictFields=[NSDictionary dictionaryWithObject:@"id,email" forKey:@"fields"];

        userData = [[Facebook sharedInstance] getuserData:dictFields];

        return userData;
    }
    @catch (NSException *exception) {
        NSLog(@"Exception Found : %@",exception);
    }
    @finally {
        
    }
    return [[NSDictionary alloc] init];
}

@end
