//
//  CustomAlertview.m
//  HeartDart
//
//  Created by Tasol on 9/2/14.
//  Copyright (c) 2014 LGOM. All rights reserved.
//

#import "CustomAlertview.h"

@implementation CustomAlertview
@synthesize btnConfirm,btnCancel;
- (id)init
{
    
    if ([[ApplicationConfiguration sharedInstance].POSTFIX_XIB isEqualToString:@"Lower"])
    {
        self=[self initWithFrame:CGRectMake(0,0,320 ,480)];
    }
    else
    {
         self=[self initWithFrame:CGRectMake(0,0,320 ,568)];
    }
    
   
    if (self)
    {
        [self setTheme];
    }
    return self;
}

#pragma mark -
#pragma mark set Theme

-(void)setTheme
{
    @try
    {
        viewAlertBG = [[UIView alloc]initWithFrame:CGRectMake(self.frame.origin.x,self.frame.origin.y,self.frame.size.width,self.frame.size.height)];
        
        [self addSubview:viewAlertBG];
        
        
        imgAlertBG = [[UIImageView alloc]initWithFrame:CGRectMake(35, (viewAlertBG.frame.size.height/2)-77.5, 250, 155)];
        imgAlertBG.image = [UIImage imageNamed:@"custom_alert_popupbox.png"];
        [viewAlertBG addSubview:imgAlertBG];
        
        lblAlertTitle = [[UILabel alloc]initWithFrame:CGRectMake(60, imgAlertBG.frame.origin.y+10, 210, 21)];
        lblAlertTitle.textColor = [ApplicationConfiguration sharedInstance].textColor;
        lblAlertTitle.font = [UIFont systemFontOfSize:16.0f];
        lblAlertTitle.textAlignment = NSTextAlignmentCenter;
        [viewAlertBG addSubview:lblAlertTitle];
        
        lblAlertMessage = [[UILabel alloc]initWithFrame:CGRectMake(42,lblAlertTitle.frame.origin.y+38, 235, 21)];
        lblAlertMessage.textColor = [UIColor grayColor];
        lblAlertMessage.font = [UIFont systemFontOfSize:10.5];
        lblAlertMessage.textAlignment = NSTextAlignmentCenter;
        [viewAlertBG addSubview:lblAlertMessage];
        
        
        btnCancel = [[UIButton alloc]initWithFrame:CGRectMake(55, lblAlertMessage.frame.origin.y+62, 105, 40)];
        [btnCancel setTitle:@"Cancel" forState:UIControlStateNormal];
        [btnCancel setTitleColor:[ApplicationConfiguration sharedInstance].textColor forState:UIControlStateNormal];
        [viewAlertBG addSubview:btnCancel];
        
        btnConfirm = [[UIButton alloc]initWithFrame:CGRectMake(160, btnCancel.frame.origin.y, 105, 40)];
        [btnConfirm setTitle:@"Confirm" forState:UIControlStateNormal];
        [btnConfirm setTitleColor:[ApplicationConfiguration sharedInstance].textColor forState:UIControlStateNormal];
        [viewAlertBG addSubview:btnConfirm];
        
        imgSepratorLine = [[UIImageView alloc]initWithFrame:CGRectMake(60, btnCancel.frame.origin.y, 200, 1)];
        imgSepratorLine.image = [UIImage imageNamed:@"line_seperator.png"];
        [viewAlertBG addSubview:imgSepratorLine];
        
        [viewAlertBG setBackgroundColor:[UIColor colorWithRed:0.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:0.75]];
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

#pragma mark -
#pragma mark Set Title and Messsage

-(void)setTheme:(NSString *)title AlertMessge:(NSString *)alertMessage
{
    @try
    {
        lblAlertMessage.text = alertMessage;
        
        lblAlertMessage.numberOfLines=0;
        lblAlertMessage.frame = CGRectMake(lblAlertMessage.frame.origin.x, lblAlertMessage.frame.origin.y, lblAlertMessage.frame.size.width, lblAlertMessage.frame.size.height*4);
        
        CGSize labelSize = [lblAlertMessage.text sizeWithFont:lblAlertMessage.font
                                       constrainedToSize:lblAlertMessage.frame.size
                                           lineBreakMode:lblAlertMessage.lineBreakMode];
        lblAlertMessage.frame = CGRectMake(
                                      lblAlertMessage.frame.origin.x, lblAlertMessage.frame.origin.y,
                                      lblAlertMessage.frame.size.width, labelSize.height);
        
        
        lblAlertMessage.textAlignment = NSTextAlignmentCenter;
        
        
        lblAlertTitle.text = title;
    }
    @catch (NSException *exception)
    {
        NSLog(@"Set Custom Alertview Exception : %@",exception);
    }
    @finally
    {
        
    }
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
