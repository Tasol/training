//
//  PhotoEffect.m
//  LGOM
//
//  Created by Kalpesh on 9/24/14.
//  Copyright (c) 2014 LGOM. All rights reserved.
//

#import "PhotoEffect.h"

@implementation PhotoEffect
- (instancetype)init
{
    self = [super init];
    if (self) {
//        self.effectArray = [[NSArray alloc] initWithObjects:@"CIPhotoEffectChrome",@"CIPhotoEffectFade",@"CIPhotoEffectInstant",@"CIPhotoEffectMono",@"CIPhotoEffectNoir",@"CIPhotoEffectProcess",@"CIPhotoEffectTonal",@"CIPhotoEffectTransfer",nil];
    }
    return self;
}
#pragma getImageWithEffect
#pragma ApplyEffectOn image with string 

#pragma CIPhotoEffectChrome
#pragma CIPhotoEffectFade
#pragma CIPhotoEffectInstant
#pragma CIPhotoEffectMono
#pragma CIPhotoEffectNoir
#pragma CIPhotoEffectProcess
#pragma CIPhotoEffectTonal
#pragma CIPhotoEffectTransfer


+(UIImage*)getImageWithEffect:(NSData*)beginImage withEffect:(NSString*)Effect{
    @try {
//        NSString *filePath =
        [[NSBundle mainBundle] pathForResource:@"viewpost_bg" ofType:@"png"];
//        NSURL *fileNameAndPath = [NSURL fileURLWithPath:filePath];
        
        // 2
//        CIImage *beginImage =
//        [CIImage imageWithContentsOfURL:fileNameAndPath];
        UIImageOrientation orient = [UIImage imageWithData:beginImage].imageOrientation;
        
        CIImage *iamge = [CIImage imageWithData:beginImage];;

        // 1
        CIContext *context = [CIContext contextWithOptions:nil];

        CIFilter *filter = [CIFilter filterWithName:Effect];
        [filter setValue:iamge forKey:kCIInputImageKey];
        
        CIImage *outputImage = [filter outputImage];
        
        // 2
        CGImageRef cgimg =
        [context createCGImage:outputImage fromRect:[outputImage extent]];

        UIImage *newImage;
        if (orient ==  UIImageOrientationRight) {
            newImage = [UIImage imageWithCGImage:cgimg scale:1 orientation:UIImageOrientationRight];

        }else{
            newImage = [UIImage imageWithCGImage:cgimg ];
        }
        
        
//        newImage = [self scaleAndRotateImage:newImage];
//        scale:3 orientation:UIImageOrientationRight
        
        CGImageRelease(cgimg);
        
        return newImage;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}
+ (UIImage *)scaleAndRotateImage:(UIImage *)image {
//    int kMaxResolution = 640; // Or whatever
    
    CGImageRef imgRef = image.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
//    if (width > kMaxResolution || height > kMaxResolution) {
//        CGFloat ratio = width/height;
//        if (ratio > 1) {
//            bounds.size.width = kMaxResolution;
//            bounds.size.height = roundf(bounds.size.width / ratio);
//        }
//        else {
//            bounds.size.height = kMaxResolution;
//            bounds.size.width = roundf(bounds.size.height * ratio);
//        }
//    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = image.imageOrientation;
    switch( 6) {
            
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    }
    else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return imageCopy;
}
+(NSMutableArray*)getImages:(NSData*)image effects:(NSArray*)effectArray{
    @try {
              NSMutableArray *imageArray = [[NSMutableArray alloc] init];
        image = UIImageJPEGRepresentation([self getThumbnail:[UIImage imageWithData:image]], 1);

        [imageArray addObject:[UIImage imageWithData:image]];
        for (NSString *effect in effectArray) {
            @autoreleasepool{
                UIImage *iamge = [self getImageWithEffect:image withEffect:effect];
                [imageArray addObject:iamge];
            }

        }
        return imageArray;
    }
    @catch (NSException *exception) {

    }
    @finally {
        
    }
}
+(UIImage*)getThumbnail:(UIImage*)image{
    @try {
//        UIImage *image = YourImageView.image;
        UIImage *tempImage = nil;
        CGSize targetSize = CGSizeMake(96,170);
        UIGraphicsBeginImageContext(targetSize);
        
        CGRect thumbnailRect = CGRectMake(0, 0, 0, 0);
        thumbnailRect.origin = CGPointMake(0.0,0.0);
        thumbnailRect.size.width  = targetSize.width;
        thumbnailRect.size.height = targetSize.height;
        
        [image drawInRect:thumbnailRect];
        
        tempImage = UIGraphicsGetImageFromCurrentImageContext();
        
        UIGraphicsEndImageContext();
        
        return tempImage;
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}

@end
