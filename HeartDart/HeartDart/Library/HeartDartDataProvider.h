//
//  HeartDartDataProvider.h
//  HeartDart
//
//  Created by tasol on 10/30/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HeartDartDataProvider : NSObject

@property (nonatomic,retain) NSMutableArray *arrFriendRequest;
@property (nonatomic,retain) NSString *stringMessage;
+ (HeartDartDataProvider*)sharedInstance;
-(BOOL)registerUserWithTaskData:(NSDictionary*)taskData imageData:(NSData*)imageData;
-(BOOL)loginUser:(NSDictionary *)taskData;
-(NSDictionary *)getProfile:(NSDictionary *)taskData;
-(BOOL)updatedProfile:(NSDictionary *)taskData;
-(NSMutableArray *)getUserList:(NSDictionary *)taskData;
-(void)responseToFriendRequest:(NSDictionary *)taskData;
-(BOOL)sendFriendRequest:(NSDictionary *)taskData;
-(NSMutableArray * )getFriendRequest:(NSDictionary *)taskData;
@end
