//
//  RequestObjectProvider.h
//  Intafy
//
//  Created by tasol on 2/24/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RequestObjectProvider : NSObject
+(NSString*)generateRequestObjectForLogin:(NSString*)extTask withTaskData:(NSDictionary*)taskData;
+(NSString*)generateRequestObjectFor:(NSString*)extTask withTaskData:(NSDictionary*)taskData extView:(NSString*)extView;
+(NSString*)generateRequestObjectForMessageWith:(NSString*)extTask withTaskData:(NSDictionary*)taskData extView:(NSString*)extView;
@end
