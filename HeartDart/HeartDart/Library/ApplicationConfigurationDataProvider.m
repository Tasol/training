//
//  ApplicationConfigurationDataProvider.m
//  Intafy
//
//  Created by tasol on 2/11/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "ApplicationConfigurationDataProvider.h"
#import "NSUtil.h"
#import "Core_joomer.h"
#import "RequestObjectProvider.h"
#import "ErrorHandler.h"
//#import "DataBase.h"


//#import "DataBase.h"

@implementation ApplicationConfigurationDataProvider
static ApplicationConfigurationDataProvider *appConfig;
+ (ApplicationConfigurationDataProvider*)sharedInstance{
    if (appConfig == nil) {
        appConfig = [[super allocWithZone:NULL] init];
        //        [self intializeComponent];
        
        
    }
    
    return appConfig;
}
-(BOOL)registerUserWithTaskData:(NSDictionary *)taskData
{
    return YES;
}
-(void)getApplicationConfigurationFromServerWithTaskData:(NSDictionary*)taskData{
    @try {
        NSDictionary *dict_task = [NSDictionary dictionaryWithObjectsAndKeys: @"applicationConfig", @"task", taskData, @"taskData", nil];
        NSString *jsonString = [dict_task JSONString];
        NSDictionary *responseData = [Core_joomer sendRequest:jsonString];
        if([[responseData valueForKey:TAG_CODE] integerValue]==200){
            [self parseConfigData:responseData];

        }else{
             [self performSelector:@selector(Alert) withObject:nil afterDelay:0.1];
        }
    }
    @catch (NSException *exception) {
    }
    @finally {
    }
}

-(void)Alert
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Server Error" message:@"Bad Data In Server Response." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];
}

-(void)parseConfigData:(NSDictionary*)dict{
    @try {
        NSDictionary *configuration = [dict objectForKey:@"configuration"];
        
        [ApplicationConfiguration sharedInstance].globalconfig =[configuration objectForKey:@"globalconfig"];
        [ApplicationConfiguration sharedInstance].extentionconfig =[configuration objectForKey:@"extentionconfig"];
        [ApplicationConfiguration sharedInstance].versioninfo=[configuration objectForKey:@"versioninfo"];
        
        ////save theme data in a database
        if([[configuration objectForKey:@"theme"] isKindOfClass:[NSArray class]]){
//            [super storeDataINDataBase:[NSDictionary dictionaryWithObject:[configuration objectForKey:@"theme"] forKey:@"theme"]];
        }

        //////save Menus in database
        if([[configuration objectForKey:@"menus"] isKindOfClass:[NSArray class]]){
//            [super storeDataINDataBase:[NSDictionary dictionaryWithObject:[configuration objectForKey:@"menus"] forKey:@"menus"]];
        }
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
}




@end