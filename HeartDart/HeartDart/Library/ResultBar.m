//
//  ProgressBar.m
//  LGOM
//
//  Created by Kalpesh on 9/15/14.
//  Copyright (c) 2014 LGOM. All rights reserved.
//

#import "ResultBar.h"

@implementation ResultBar
@synthesize timeRemaining,percent;
@synthesize strTime,strVote,totalVote,yesVote,noVote;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.backgroundColor = [UIColor whiteColor];
        
        // Determine our start and stop angles for the arc (in radians)
        startAngle = M_PI * 0;
        endAngle = startAngle + (M_PI * 2);
        self.percent=0;
        self.strTime=@"";
        self.strVote=@"";
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
 
    
    UIBezierPath* bezierPath = [UIBezierPath bezierPath];

    if (yesVote==noVote) {
        // Text Drawing for time
        CGRect textRect1 = CGRectMake(0, 70, 200, 30);
        [[ApplicationConfiguration sharedInstance].textColor setFill];
        [@"SPLIT" drawInRect: textRect1 withFont: [UIFont fontWithName: @"Helvetica-Bold" size: 30] lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter];

        
        // Text Drawing for vore
        CGRect textRect = CGRectMake(0, 100, 200, 30);
        [[UIColor whiteColor] setFill];
        [@"VOTE" drawInRect: textRect withFont: [UIFont fontWithName: @"Helvetica-Bold" size: 30] lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter];
    }else if (totalVote==yesVote){
        CGRect textRect = CGRectMake(0, 89, 200, 30);
        [[UIColor whiteColor] setFill];
        [@"YES" drawInRect: textRect withFont: [UIFont fontWithName: @"Helvetica-Bold" size: 30] lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter];
        
    }else if (totalVote==noVote){
        CGRect textRect = CGRectMake(0, 89, 200, 30);
        [[ApplicationConfiguration sharedInstance].textColor setFill];
        [@"NO" drawInRect: textRect withFont: [UIFont fontWithName: @"Helvetica-Bold" size: 30] lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter];

    }else {
        // Text Drawing for time
        CGRect textRect1 = CGRectMake(0, 65, 200, 20);
        [[ApplicationConfiguration sharedInstance].textColor setFill];
        [[NSString stringWithFormat:@"%d NO Votes",noVote] drawInRect: textRect1 withFont: [UIFont fontWithName: @"Helvetica-Bold" size: 20] lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter];
        
        
        // Text Drawing for vore
        CGRect textRect = CGRectMake(0, 114, 200, 20);
        [[UIColor whiteColor] setFill];
        [[NSString stringWithFormat:@"%d YES Votes",yesVote] drawInRect: textRect withFont: [UIFont fontWithName: @"Helvetica-Bold" size: 20] lineBreakMode: NSLineBreakByWordWrapping alignment: NSTextAlignmentCenter];
    }
    
    
    percent = (float)(((float)totalVote-(float)noVote)*360.0)/totalVote;
    
    
    // Create our arc, with the correct angles
    [bezierPath addArcWithCenter:CGPointMake(rect.size.width / 2, rect.size.height / 2)
                          radius:((rect.size.width / 2)-10)
                      startAngle: DEGREES_TO_RADIANS(-90)
                        endAngle:DEGREES_TO_RADIANS(percent-90)
                       clockwise:YES];
    
    // Set the display for the path, and stroke it
    bezierPath.lineWidth = 15;
    [[UIColor whiteColor] setStroke];
    [bezierPath stroke];
    [bezierPath closePath];
    
    // Create our arc, with the correct angles
    bezierPath = [UIBezierPath bezierPath];
    [bezierPath addArcWithCenter:CGPointMake(rect.size.width / 2, rect.size.height / 2)
                          radius:((rect.size.width / 2)-10)
                      startAngle: DEGREES_TO_RADIANS(percent-90)
                        endAngle:DEGREES_TO_RADIANS(360-90)
                       clockwise:YES];
    
    // Set the display for the path, and stroke it
    bezierPath.lineWidth = 15;
    [[ApplicationConfiguration sharedInstance].textColor setStroke];
    [bezierPath stroke];
}
@end
