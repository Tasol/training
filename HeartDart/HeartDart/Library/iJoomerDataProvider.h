//
//  iJoomerDataProvider.h
//  Intafy
//
//  Created by tasol on 2/22/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ErrorHandler.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Facebook.h"
@interface iJoomerDataProvider : NSObject



@property (nonatomic,retain) Facebook *facebookObj;

-(void)storeDataINDataBase :(NSDictionary *)dict;

-(BOOL)registerUserWithTaskData:(NSDictionary*)taskData;

-(BOOL)loginUserWithTaskData:(NSDictionary*)taskData;

-(BOOL)checkLogin;
-(BOOL) validateLogin;


-(NSDictionary*)getUserProfileFromServer;
-(BOOL)logoutUserWithTaskData:(NSDictionary*)taskData;

+(BOOL)forgotLogin:(NSDictionary *)taskData;
-(NSDictionary*)retrieveUserInfoFromFacebook;
@end

