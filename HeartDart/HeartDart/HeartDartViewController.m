//
//  HeartDartViewController.m
//  HeartDart
//
//  Created by tasol on 10/30/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "HeartDartViewController.h"
@interface HeartDartViewController ()

@end

@implementation HeartDartViewController




- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)showAlert:(NSString *)title Content:(NSString *)bodyText
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:bodyText
                                                   delegate:nil
                                          cancelButtonTitle:@"Close" otherButtonTitles: nil];
    [alert setContentMode:UIViewContentModeScaleAspectFit];
    [alert show];
}

-(void)showAlert:(NSString *)title Content:(NSString *)bodyText Parent:(id)parent
{
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title message:bodyText
                                                   delegate:nil
                                          cancelButtonTitle:@"OK" otherButtonTitles: nil];
    //
    //    AlertViewPopUP *alertPopUp=[[AlertViewPopUP alloc]initWIthTarget:self andTitle:title andMessage:bodyText];
    //    [self.view addSubview:alertPopUp];
    
    [alert setContentMode:UIViewContentModeScaleAspectFit];
    if(parent!=nil)
    {
        [((UIViewController*)parent).view addSubview:alert];
        alert.delegate=parent;
        alert.tag=560;
    }
    [alert show];
}
-(void)callBack:(id)Class image:(id)image originalImage:(id)originalImage
{
    
}
-(void)callBack:(id)class
{
    
}
-(void)callBack:(id)Class withObject:(id)sender
{
    
}


@end
