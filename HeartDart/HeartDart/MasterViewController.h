//
//  MasterViewController.h
//  HeartDart
//
//  Created by tasol on 10/29/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterViewController : HeartDartViewController<UITableViewDataSource,UITableViewDelegate>
{
    IBOutlet UITableView *tblStream;
    IBOutlet UIButton *btnFriend;
    IBOutlet UIButton *btnFollowing;
    IBOutlet UIButton *btnRandom;
    
    NSMutableArray *arrStream;
}

-(IBAction)btnFriendPressed:(id)sender;
-(IBAction)btnFollowingPressed:(id)sender;
-(IBAction)btnRandomPressed:(id)sender;
@end
