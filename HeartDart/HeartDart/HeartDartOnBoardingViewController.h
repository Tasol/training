//
//  RevOnBoardingViewController.h
//  iJoomer
//
//  Created by tasol on 3/20/14.
//
//

#import <UIKit/UIKit.h>

@interface HeartDartOnBoardingViewController : HeartDartViewController<UIScrollViewDelegate>

{
    IBOutlet UILabel *lblStatusBar;
    
    IBOutlet UIActivityIndicatorView *spinner;
    IBOutlet UIScrollView *scrolView;
    IBOutlet UIPageControl *pageControl;
    IBOutlet UIButton *btnLogin;
    
    UIImageView *imgBoardind1;
    UILabel *lblBoardingTop1;
    UILabel *lblBoardingBottom1;
    
    UIImageView *imgBoardind2;
    
    UIImageView *imgBoardind3;
    
    NSTimer *timer1;
    NSTimer *timer2;
}

-(IBAction)btnLoginPressed:(id)sender;
-(IBAction)changePage:(id)sender;

@end
