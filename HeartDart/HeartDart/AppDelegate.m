//
//  AppDelegate.m
//  HeartDart
//
//  Created by tasol on 10/29/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "AppDelegate.h"
#import "HeartDartOnBoardingViewController.h"
#import "MasterViewController.h"
#import "iJoomerDataProvider.h"
#import "LoginViewController.h"

@implementation AppDelegate
@synthesize window;
@synthesize btnSideMenu;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
//    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    [[ApplicationConfiguration sharedInstance]genarateServerURL];
    
//    [self validateLogin];
    MasterViewController *controller=[[MasterViewController alloc]init];
    self.navigationController=[[UINavigationController alloc]initWithRootViewController:controller];

    
      window.rootViewController = self.navigationController;
    
    [self.navigationController.navigationBar setHidden:YES];
    
    
    
    [self setSlideMenu];
    
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

-(void)setSlideMenu
{
    
    @try
    {

        btnSideMenu.hidden=YES;
        
        [viewSideMenu bringSubviewToFront:btnSideMenu];
        [window addSubview:btnSideMenu];
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
            
        {
            CGSize result = [[UIScreen mainScreen] bounds].size;
            
            if(result.height == 480)
            {
                tblSideMenu.scrollEnabled=YES;
                tblSideMenu.frame = CGRectMake(0, 70, 240, 380);
            }
            else  if(result.height == 568)
            {
                tblSideMenu.scrollEnabled=NO;
                tblSideMenu.frame = CGRectMake(0, 70, 240, 555);
            }
        }
//             tblSideMenu.frame = CGRectMake(0, 80, 275, tblSideMenu.frame.size.height-80);
        
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(singleTapPressed)];
        singleTap.delegate = self;
        [singleTap setNumberOfTapsRequired:1];
        [viewTap addGestureRecognizer:singleTap];
//        [tblSideMenu setHidden:YES];
        
        [window addSubview:viewSideMenu];
        [viewSideMenu addSubview:viewSide];
        [viewSide addSubview:tblSideMenu];
        [viewSideMenu addSubview:viewTap];
        
//        [self setSlideMenu];
//        [self btnSideMenuPressed:0];
        [btnSideMenu addTarget:self action:@selector(btnSideMenuPressed:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
}

-(void)singleTapPressed
{
    [self btnSideMenuPressed:0];
}

-(IBAction)btnSideMenuPressed:(id)sender
{
    @try
    {
        arrSideMenu=[[NSMutableArray alloc]initWithObjects:@"Stream",@"My Messages",@"Prayer Request",@"Friends",@"Library",@"Profile",@"Share",@"Setting",@"Logout", nil];
        
        [UIView beginAnimations:@"toggleSideMenu" context:NULL];
        [UIView setAnimationDelegate:self];
        [UIView setAnimationDuration:0.3f];
        
        if (viewSideMenu.frame.origin.x==-320)
        {
            viewSideMenu.frame=CGRectMake(0, 30, 320, 555);
            btnSideMenu.frame=CGRectMake(240, 30, 30, 30);
            [window bringSubviewToFront:btnSideMenu];
//            viewTap.frame=CGRectMake(240, 0, 40, 555);
            UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-1];
            [viewController.view setFrame:CGRectMake(240, viewController.view.frame.origin.y, viewController.view.frame.size.width, viewController.view.frame.size.height)];

        }
        
        else if(viewSideMenu.frame.origin.x==0)
        {
            viewSideMenu.frame=CGRectMake(-320, 30, 320, 555);
            btnSideMenu.frame=CGRectMake(20, 30, 30, 30);
            [window bringSubviewToFront:btnSideMenu];
//            viewTap.frame=CGRectMake(0, 0, 40, 555);
            UIViewController *viewController = [self.navigationController.viewControllers objectAtIndex:self.navigationController.viewControllers.count-1];
            [viewController.view setFrame:CGRectMake(0, viewController.view.frame.origin.y, viewController.view.frame.size.width, viewController.view.frame.size.height)];
        }
        
        
        
        
//        if (viewSide.frame.origin.x == 0)
//        {
//            viewSide.frame = CGRectMake(-viewSide.frame.size.width, viewSide.frame.origin.y, viewSide.frame.size.width, viewSide.frame.size.height);
//            btnSideMenu.frame = CGRectMake(10, btnSideMenu.frame.origin.y, btnSideMenu.frame.size.width, btnSideMenu.frame.size.height);
//            
//        }
//        else
//        {
//            viewSide.frame = CGRectMake(0, viewSide.frame.origin.y, viewSide.frame.size.width, viewSide.frame.size.height);
//            btnSideMenu.frame = CGRectMake(viewSideMenu.frame.size.width+10, btnSideMenu.frame.origin.y, btnSideMenu.frame.size.width, btnSideMenu.frame.size.height);
//            
        
//        }
        [tblSideMenu setHidden:NO];
      [tblSideMenu reloadData];
        [UIView commitAnimations];
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [arrSideMenu count];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 50.0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
        static NSString *CellIdentifier = @"Cell";
        
        UIImageView *imgArrow = [[UIImageView alloc] init];
        imgArrow.image = [UIImage imageNamed:@"menu_arrow.png"];
        imgArrow.frame = CGRectMake(210, 15, 15, 15);
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        cell.textLabel.text = [arrSideMenu objectAtIndex:indexPath.row];
        cell.textLabel.textColor=[UIColor whiteColor];
        cell.textLabel.font=[UIFont fontWithName:nil size:25.0];
        
        
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell.contentView setBackgroundColor:[UIColor clearColor]];
        
        [cell addSubview:imgArrow];
        
        return cell;
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception:- %@",exception);
    }
    @finally
    {
    }
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    @try
    {
        UIViewController *controller = nil;
        
        NSString *strControllerName = [arrSideMenu objectAtIndex:indexPath.row];
        
        if ([strControllerName isEqualToString:@"Logout"])
        {
            [self btnSideMenuPressed:0];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Are you sure, you want to logout" delegate:self cancelButtonTitle:@"cancel" otherButtonTitles:@"ok", nil];
            alert.tag = 2;
            [alert show];
            
        }
        else
        {
            NSDictionary *plistDict = [[NSDictionary alloc] initWithContentsOfFile:[[[NSBundle mainBundle] bundlePath] stringByAppendingPathComponent:@"Data.plist"]];
            
            NSArray *arrControllerList = [plistDict objectForKey:@"ControllerList"];
            
            for (int i=0; i<arrControllerList.count; i++)
            {
                NSDictionary *dict = [arrControllerList objectAtIndex:i];
                NSString *key = [[dict allKeys] objectAtIndex:0];
                NSArray *arrtemp = [dict objectForKey:key];
                
                if ([[[arrtemp objectAtIndex:0] lowercaseString] isEqualToString:[strControllerName lowercaseString]])
                {
                    Class controllerClass = NSClassFromString(key);
                    
                    controller = [[controllerClass alloc] init];
                    
                    [self.navigationController pushViewController:controller animated:YES];
                    
                    
                    
                    [self btnSideMenuPressed:0];
                    return;
                }
            }
        }
    }
    @catch (NSException *exception)
    {
        NSLog(@"Exception:- %@",exception);
    }
    @finally
    {
    }
}

-(void)validateLogin
{
    NSUserDefaults *standardUserDefault=[NSUserDefaults standardUserDefaults];
    NSString *email=[standardUserDefault valueForKey:@"email"];
    NSString *userPass=[standardUserDefault valueForKey:@"password"];
//    NSInteger appType=[standardUserDefault integerForKey:@"appType"];
    BOOL isLoggedIn=[standardUserDefault boolForKey:@"isLoggedIn"];
    
    if(email && userPass && isLoggedIn)
    {
//        [ApplicationConfiguration sharedInstance].isLogedIn=YES;
        MasterViewController *controller=[[MasterViewController alloc]init];
        self.navigationController=[[UINavigationController alloc]initWithRootViewController:controller];
//            btnSideMenu.hidden=NO;
        
    }
           else
    {
        [ApplicationConfiguration sharedInstance].isLogedIn=NO;
        HeartDartOnBoardingViewController *controller=[[HeartDartOnBoardingViewController alloc]init];
        self.navigationController=[[UINavigationController alloc]initWithRootViewController:controller];
//        btnSideMenu.hidden=YES;
           }
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1)
    {
        LoginViewController *controller = [[LoginViewController alloc] init];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else if (alertView.tag == 2)
    {
        if (buttonIndex == 1)
        {
//            [self startAnimating];
            [self performSelector:@selector(logout) withObject:nil afterDelay:0.1];
        }
    }

}

- (void)logout
{
    @try
    {
        NSLog(@"Logout button Pressed");
        
        //        UIAlertView *alertView = [[UIAlertView alloc]initWithTitle:@"" message:@"Are you sure you want to logout?" delegate:self cancelButtonTitle:@"Confirm" otherButtonTitles:@"Cancel", nil];
        //        [alertView show];
        
        iJoomerDataProvider *controller=[[iJoomerDataProvider alloc]init];
        NSDictionary *dict=[[NSDictionary alloc]initWithObjectsAndKeys:[ApplicationConfiguration sharedInstance].deviceToken,@"devicetoken",@"iphone",@"type", nil];
        if([controller logoutUserWithTaskData:dict])
        {
            LoginViewController *controller_=[[LoginViewController alloc]init];
            [self.navigationController pushViewController:controller_ animated:YES];
        }
//        [self btnSideMenuPressed:sender];
    }
    @catch (NSException *exception)
    {
        NSLog(@"Logoutß button Exception");
    }
    @finally {
        
    }
}



@end
