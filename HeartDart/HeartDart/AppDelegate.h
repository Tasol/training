//
//  AppDelegate.h
//  HeartDart
//
//  Created by tasol on 10/29/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate,UITableViewDelegate,UITableViewDataSource,UIGestureRecognizerDelegate,UIAlertViewDelegate>

{
    IBOutlet UIView *viewSide;
  IBOutlet UIView *viewTap;
    IBOutlet UIView *viewSideMenu;
    IBOutlet UITableView *tblSideMenu;
    IBOutlet UIButton *btnSideMenu;
    IBOutlet UIActivityIndicatorView *spinner;
    NSMutableArray *arrSideMenu;
}

@property (strong,nonatomic)IBOutlet UIButton *btnSideMenu;

@property (strong, nonatomic)IBOutlet UIWindow *window;

@property(nonatomic,strong)UINavigationController *navigationController;

-(IBAction)btnSideMenuPressed:(id)sender;

-(void)setSlideMenu;

@end
