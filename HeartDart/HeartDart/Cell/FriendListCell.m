//
//  FriendListCell.m
//  HeartDart
//
//  Created by tasol on 11/5/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "FriendListCell.h"

@implementation FriendListCell

@synthesize postDetails;
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self setTheme];
    }
    return self;
}

-(void)setTheme
{
    @try
    {
        viewHighlight=[[UIView alloc]initWithFrame:CGRectMake(0, 0, 92, 92)];
        
        [self addSubview:viewHighlight];
        
        imgViewHighlightBG=[[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 92, 92)];
        
        [viewHighlight addSubview:imgViewHighlightBG];
        
        imgPhoto=[[UIImageView alloc]initWithFrame:CGRectMake(15, 8, 60, 60)];
        imgPhoto.layer.borderWidth=2.0f;
        imgPhoto.backgroundColor=[UIColor whiteColor];
        imgPhoto.layer.borderColor=[UIColor whiteColor].CGColor;
        [viewHighlight addSubview:imgPhoto];
        
        lblName=[[UILabel alloc]initWithFrame:CGRectMake(0,73, 92, 15)];
        lblName.textColor=[UIColor blackColor];
        lblName.font=[UIFont fontWithName:nil size:12.0];
        lblName.textAlignment=NSTextAlignmentCenter;
        [viewHighlight addSubview:lblName];
        
        imgPhoto.clipsToBounds=YES;
        imgPhoto.layer.cornerRadius=(imgPhoto.frame.size.width)/2;
        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    lblName.text=@"";
    imgPhoto.image=nil;
}

-(void)reloadCell
{
    @try
    {
        imgViewHighlightBG.backgroundColor=[UIColor purpleColor];
        //        viewHighlight.backgroundColor=[self colorWithHexString:[postDetails valueForKey:@"moodColor"]];
        imgViewHighlightBG.backgroundColor=[NSUtil colorWithHexString:[postDetails valueForKey:@"moodColor"]];
        imgPhoto.image=[NSUtil cropImageFromMiddle:[NSUtil getImageFromURL:[postDetails valueForKey:@"avatar"]]];
        
        lblName.text=[postDetails valueForKey:@"username"];
        lblName.textColor=[UIColor blackColor];
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}


@end
