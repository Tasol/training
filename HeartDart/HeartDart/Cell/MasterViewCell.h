//
//  MasterViewCell.h
//  HeartDart
//
//  Created by tasol on 10/31/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MasterViewCell : UITableViewCell
{
    IBOutlet UILabel *lblMessage;
    IBOutlet UILabel *lblAuthour;

}

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier target:(id)target;
-(void)setTheme;
-(void)reload;
@end
