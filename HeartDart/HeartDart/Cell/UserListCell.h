//
//  FriendListCell.h
//  HeartDart
//
//  Created by tasol on 11/4/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserListCell : UICollectionViewCell
{
    UIView *viewHighlight;
    UIImageView *imgPhoto;
    UIImageView *imgViewHighlightBG;
    UILabel *lblName;
    
    NSDictionary *postDetails;
}

@property (nonatomic,retain) NSDictionary *userDetails;
-(void)reloadCell;
@end
