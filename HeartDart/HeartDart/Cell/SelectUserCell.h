//
//  SelectUserCell.h
//  HeartDart
//
//  Created by tasol on 11/3/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectUserCell : UITableViewCell
{
    IBOutlet UILabel *lblUserName;
    IBOutlet UILabel *lblUserStatus;
    
    IBOutlet UIImageView *imgProfile;
}
@property(nonatomic,strong)NSDictionary *userDetail;
-(void)reloadCell;

@end
