//
//  SelectUserCell.m
//  HeartDart
//
//  Created by tasol on 11/3/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import "SelectUserCell.h"

@implementation SelectUserCell
@synthesize userDetail;




-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    @try
    {
        self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
        if (self)
        {
            [self setFrame];
        }
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    return self ;
}


#pragma mark 
#pragma mark Set Frame

-(void)setFrame
{
    @try
    {
        imgProfile = [[UIImageView alloc]initWithFrame:CGRectMake(12, 15, 70, 70)];
        imgProfile.layer.cornerRadius = imgProfile.frame.size.height/2;
        imgProfile.clipsToBounds=YES;
        [self addSubview:imgProfile];
        
        
        lblUserName = [[UILabel alloc]initWithFrame:CGRectMake(100, 25, 90, 20)];
        [self addSubview:lblUserName];
        
        lblUserStatus = [[UILabel alloc]initWithFrame:CGRectMake(100, 45, 200, 20)];
        [self addSubview:lblUserStatus];

        
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
}
#pragma mark
#pragma mark Reload Cell with new values

-(void)reloadCell
{
    @try
    {
        imgProfile.image = [userDetail valueForKey:@""];
        lblUserName.text = @"Username";
        lblUserStatus.text = @"User status";
    }
    @catch (NSException *exception)
    {
        
    }
    @finally
    {
        
    }
    
}


@end
