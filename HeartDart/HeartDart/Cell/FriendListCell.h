//
//  FriendListCell.h
//  HeartDart
//
//  Created by tasol on 11/5/14.
//  Copyright (c) 2014 tasol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FriendListCell : UICollectionViewCell
{
    UIView *viewHighlight;
    UIImageView *imgPhoto;
    UIImageView *imgViewHighlightBG;
    UILabel *lblName;
    
    NSDictionary *postDetails;
}

@property (nonatomic,retain) NSDictionary *postDetails;
-(void)reloadCell;
@end
